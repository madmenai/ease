import math
import numpy as np
from enum import Enum
from collections import Counter
from wordfreq import top_n_list
from typing import List, Tuple, Union
from scipy.stats import linregress, entropy


def calc_counts(sentences: List[str]):
    counter = Counter()
    total_count = 0
    for sentence in sentences:
        for word in sentence.split():
            counter[word] += 1
            total_count += 1
    return counter, total_count


def log(x: float) -> float:
    return math.log(x, 10)


def count_pruned_vocab_size(counter, freq: int) -> Tuple[int, int]:
    vocab_size_pruned = 0
    total_count = 0
    for _, count in counter.most_common():
        if count < freq:
            break
        vocab_size_pruned += 1
        total_count += count
    return vocab_size_pruned, total_count


def calc_oov_percentage(counter, num_words: int) -> float:
    common_words = set(top_n_list('en', 10000))
    oov_count = 0
    for word, count in counter.most_common():
        if word not in common_words:
            oov_count += count
    return oov_count / num_words


def calc_zipf(counter, num_words: int) -> Tuple[float, float]:
    x = []
    y = []
    for i, _, count in enumerate(counter.most_common()):
        x.append(log(i + 1))
        y.append(log(count / num_words))
    slope, intercept, r_value, p_value, std_err = linregress(np.array(x), np.array(y))
    return slope, r_value


def calc_vocab_size(sentences: List[str]):
    counter, num_words = calc_counts(sentences)
    oov_percentage = calc_oov_percentage(counter, num_words)
    zipf_slope, zipf_rvalue = calc_zipf(counter, num_words)
    log_num_words = log(num_words)
    vocab_size = len(counter)
    log_vocab_size = log(vocab_size)
    vocab_size_5_pruned, num_words_5_pruned = count_pruned_vocab_size(counter, 5)
    log_vocab_size_5_pruned = log(vocab_size_5_pruned)
    log_num_words_5_pruned = log(num_words_5_pruned)
    vocab_size_10_pruned, num_words_10_pruned = count_pruned_vocab_size(counter, 10)
    log_num_words_10_pruned = log(num_words_10_pruned)
    log_vocab_size_10_pruned = log(vocab_size_10_pruned)
    num_sentences = len(sentences)
    log_num_sentences = log(num_sentences)
    vocab_size_div_num_sentences = vocab_size / num_sentences
    num_words_div_num_sentencs = num_words / num_sentences
    sentence_lengths = np.array([len(sentence.split()) for sentence in sentences])
    max_sentence_length = np.max(sentence_lengths)
    min_sentence_length = np.min(sentence_lengths)
    std_sentence_length = np.std(sentence_lengths)
    return vocab_size, log_vocab_size, vocab_size_5_pruned, \
           log_vocab_size_5_pruned, vocab_size_10_pruned, log_vocab_size_10_pruned, \
           num_sentences, log_num_sentences, num_words, log_num_words, \
           num_words_5_pruned, log_num_words_5_pruned, num_words_10_pruned, log_num_words_10_pruned, \
           vocab_size_div_num_sentences, num_words_div_num_sentencs, max_sentence_length, \
           min_sentence_length, std_sentence_length, oov_percentage, zipf_slope, zipf_rvalue


class TargetType(Enum):
    # SentenceLevel
    SL_REGRESSION = 0
    SL_CLASSIFICATION = 1
    SL_MULTI_CLASSIFICATION = 2
    # TokenLevel
    TL_REGRESSION = 3
    TL_CLASSIFICATION = 4
    TL_MULTI_CLASSIFICATION = 5


class InputType(Enum):
    ONE_SENTENCE = 0  # sentence -> target
    TWO_SENTENCE = 1  # sentence1, sentence2 -> target
    SELECT_SENTENCE = 2  # sentence1, sentence2/sentence3/sentence4
    QUESTION_CONTEXT = 3


def analyze_targets(targets: List[Union[int, float, List[int], List[float], List[List[int]]]], target_type: TargetType):
    # calc_num_classes, mean_num_classes_per_item, entropy, min prob, max prob
    if target_type == TargetType.SL_REGRESSION or target_type == TargetType.TL_REGRESSION:  # List[(List)float]
        num_classes = 0
        mean_classes_per_target = 1.0
        min_prob = 1.0
        max_prob = 1.0
        target_entropy = 0.0
    if target_type == TargetType.SL_CLASSIFICATION:
        num_classes = len(set(targets))
        mean_classes_per_target = 1.0
        dist = Counter()
        for target in targets:
            dist[target] += 1
        dist = np.array([count / len(targets) for word, count in dist.most_common()])
        min_prob = np.min(dist)
        max_prob = np.max(dist)
        target_entropy = entropy(dist)
    elif target_type == TargetType.SL_MULTI_CLASSIFICATION or target_type == TargetType.TL_CLASSIFICATION:
        num_classes = set()
        classes_per_target = []
        dist = Counter()
        total_count = 0
        for target in targets:
            total_count += len(target)
            num_classes += set(target)
            classes_per_target.append(len(target))
            for t in target:
                dist[t] += 1
        num_classes = len(num_classes)
        mean_classes_per_target = np.mean(np.array(classes_per_target))
        dist = np.array([count / total_count for word, count in dist.most_common()])
        min_prob = np.min(dist)
        max_prob = np.max(dist)
        target_entropy = entropy(dist)
    else:  # target_type == TargetType.TL_MULTI_CLASSIFICATION:
        num_classes = set()
        classes_per_target = []
        dist = Counter()
        total_count = 0
        for target_ in targets:
            for target in target_:
                total_count += len(target)
                num_classes += set(target)
                classes_per_target.append(len(target))
                for t in target:
                    dist[t] += 1
        num_classes = len(num_classes)
        mean_classes_per_target = np.mean(np.array(classes_per_target))
        dist = np.array([count / total_count for word, count in dist.most_common()])
        min_prob = np.min(dist)
        max_prob = np.max(dist)
        target_entropy = entropy(dist)
    return num_classes, mean_classes_per_target, min_prob, max_prob, target_entropy

# first method:
#   matrix = for each task find the best algorithm and hyper params
#   algorithm / task    task_1, task_2, task_3
#   best_alg_on_task_1   0.98 ,  0.65 , 0.43
#   best_alg_on_task_2   0.91 ,  0.78 , 0.89
#   best_alg_on_task_3   0.92 ,  0.66 , 0.90
#   task => meta_features => based on meta features and alg properties find KNN

# second method:
#   matrix = for a bunch of random algorithms and random hyper params
#   algorithm / task  task_1, task_2, task_3
#   rand_alg_hyper_1   0.98 ,  0.65 , 0.43
#   rand_alg_hyper_2   0.91 ,  0.78 , 0.89
#   rand_alg_hyper_3   0.92 ,  0.66 , 0.90
#   task => run some of the matrix algorithms and based on them fill in the blanks and find KNN

# hybrid method:
#  both best algorithms and random ones?
#  A.200 pipelines * each task => 200 pipelines with best hyper params based on each task
#  B.200 pipelines * 10 logical hyper params => 2000 pipeline results for each task

# => tune hyper params

# random(static), random with meta, best(static), best with meta features, best and random with meta features, best and random and matrix with meta features
