import os
import sys
import gzip
import torch
import shutil
import pickle
import zipfile
import tarfile
from tqdm import tqdm
from collections import OrderedDict
from urllib.request import urlretrieve
from torchtext.utils import reporthook
from torch.nn.utils.rnn import PackedSequence
from typing import Optional, Any, List, TypeVar, Iterable, Union, NewType

MaybePacked = NewType('MaybePacked', Union[torch.FloatTensor, PackedSequence])
supported_compressed_formats = ['.tar.gz', '.tar.bz2', '.tar.xz', '.gz', '.zip']


def download(url: str, target_dir: str, content: Optional[str] = None, decompress: bool = True) -> str:
    mkdir(target_dir)
    destination = os.path.basename(url)
    args_pos = destination.rfind('?')
    if args_pos != -1:
        destination = destination[:args_pos]
    if content is None:
        content = destination
        if decompress:
            for postfix in supported_compressed_formats:
                if destination.endswith(postfix):
                    content = destination[:-len(postfix)]
                    break
    content = os.path.join(target_dir, content)
    if os.path.exists(content):
        return content
    destination = os.path.join(target_dir, destination)
    if not os.path.exists(destination):
        with tqdm(unit='B', unit_scale=True, miniters=1, desc=destination) as t:
            urlretrieve(url, destination, reporthook=reporthook(t))
    if decompress:
        if destination.endswith('.tar.gz') or destination.endswith('.tar.bz2') or destination.endswith('.tar.xz'):
            with tarfile.open(destination) as tf:
                tf.extractall(target_dir)
        elif destination.endswith('.zip'):
            with zipfile.ZipFile(destination, "r") as zf:
                zf.extractall(target_dir)
        elif destination.endswith('.gz'):
            with gzip.open(destination, 'rb') as f_in:
                with open(destination[:-3], 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)
    if content != destination:
        os.remove(destination)
    return content


def load_pkl(file_address: str, is_python2: bool = False) -> Optional[Any]:
    if os.path.exists(file_address):
        with open(file_address, 'rb') as f:
            return pickle.load(f, encoding='latin1' if is_python2 else 'ASCII')
    return None


def save_pkl(file_address: str, obj: Any) -> None:
    with open(file_address, 'wb') as f:
        pickle.dump(obj, f, protocol=4)


def torch_load(file_address: str, map_location: str = 'cpu') -> Optional[OrderedDict]:
    if os.path.exists(file_address):
        return torch.load(file_address, map_location=map_location)
    return None


def mkdir(directory: str) -> None:
    if not os.path.exists(directory):
        os.makedirs(directory)


def set_device(device: Union[str, torch.device]) -> torch.device:
    if isinstance(device, str):
        return torch.device(device)
    return device


T = TypeVar('T')


def listify(thing: Optional[Union[Iterable[T], T]]) -> List[T]:
    if isinstance(thing, (list, tuple)):
        return [item for item in thing if item is not None]
    if thing is None:
        return []
    return [thing]


def is_packed(tensor: MaybePacked) -> bool:
    return isinstance(tensor, PackedSequence)


class AddPath:
    def __init__(self, path):
        self.path = path

    def __enter__(self):
        sys.path.insert(0, self.path)

    def __exit__(self, exc_type, exc_value, traceback):
        try:
            sys.path.remove(self.path)
        except ValueError:
            pass
