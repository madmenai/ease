import torch
import numpy as np
from typing import Optional, Union, List
from allennlp.modules.elmo import Elmo, batch_to_ids

from model import Model
from utils import download
from pooling.pooling import Pooling
from token_embedding.embedding import DictToken2Id
from normalization.normalization import NamedTokenizer

_options_file = 'https://s3-us-west-2.amazonaws.com/allennlp/models/elmo/2x4096_512_2048cnn_2xhighway/elmo_2x4096_512_2048cnn_2xhighway_options.json'
_weight_file = 'https://s3-us-west-2.amazonaws.com/allennlp/models/elmo/2x4096_512_2048cnn_2xhighway/elmo_2x4096_512_2048cnn_2xhighway_weights.hdf5'


class ELMO(Model):
    """
    ELMO: Deep contextualized word representations
    paper: https://arxiv.org/abs/1802.05365
    pytorch_code: https://github.com/allenai/allennlp/blob/master/allennlp/modules/elmo.py
    """

    def __init__(self, *, pooling_methods: Optional[Union[Pooling, List[Pooling]]], return_numpy: bool = True,
                 dropout: float = 0.5, num_layers: int = 3, vocab_to_cache: Optional[DictToken2Id] = None,
                 batch_size: int = 64, cache: str = './cache/elmo') -> None:
        self.char_input = vocab_to_cache is None
        lower = False  # TODO check
        self.unk_index = 0  # TODO check
        super().__init__(return_numpy=return_numpy, batch_size=batch_size, encoder=None,
                         normalizer=NamedTokenizer(lower=lower, tokenizer_name='space'), lookups=None,
                         pooling_methods=pooling_methods)
        if vocab_to_cache is not None:
            for special_token in {'<S>', '</S>', '<UNK>'}:
                if special_token not in vocab_to_cache:
                    vocab_to_cache.dictionary[special_token] = len(vocab_to_cache.dictionary)
        vocab_to_cache_list = [k for k in
                               vocab_to_cache.dictionary.keys()] if vocab_to_cache is not None else vocab_to_cache
        self.encoder = Elmo(download(_options_file, cache), download(_weight_file, cache), num_layers,
                            requires_grad=False, dropout=dropout, vocab_to_cache=vocab_to_cache_list)
        self.vocab_to_cache = None if vocab_to_cache is None else {i: v for i, v in enumerate(vocab_to_cache_list)}
        if self.vocab_to_cache is not None:
            self.unk_index = self.vocab_to_cache['<UNK>']

    def forward(self, sentences: List[str], is_training_data: bool = True) -> Union[torch.FloatTensor, np.array]:
        result = []
        for i in range(0, len(sentences), self.batch_size):
            preprocessed_sentences = self.normalizer(sentences[i:i + self.batch_size])
            lengths = torch.LongTensor([len(sen) for sen in preprocessed_sentences]).to(self.device)
            if self.char_input:
                character_ids = batch_to_ids(preprocessed_sentences).to(self.device)
                outputs = self.encoder(character_ids)
            else:
                res = []
                for sentence in preprocessed_sentences:
                    sen = []
                    for token in sentence:
                        index = self.vocab_to_cache[token]
                        if index is not None:
                            sen.append(index)
                        else:
                            sen.append(self.unk_index)
                    res.append(sen)
                outputs = self.encoder(res)
            outputs = torch.cat(outputs['elmo_representations'], dim=-1)  # B, T, C
            if self.pooling_methods:
                outputs = torch.cat([p.process(outputs, lengths, preprocessed_sentences) for p in self.pooling_methods],
                                    dim=1)
                result.append(outputs.detach() if not self.trainable or self.return_numpy else outputs)
                continue
            outputs = [outputs[i, :lengths[i]] for i in range(outputs.shape[0])]
            if not self.trainable or self.return_numpy:
                outputs = [o.detach() for o in outputs]
            result.extend(outputs)
        if self.is_sentence_embedding:
            return np.concatenate(result, axis=0) if self.return_numpy else torch.cat(result, dim=0)
        else:
            return result
