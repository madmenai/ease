import os
import re
import json
import torch
import numpy as np
from typing import Union, Optional, List

from model import Model
from utils import download
from pooling.pooling import Pooling
from token_embedding.lookup import LookUp
from encoder.transformer import TransformerEncoder
from token_embedding.unk_handler import ConstantUnkHandler
from normalization.transformer import TransformerTokenizer
from token_embedding.embedding import EmbeddingWithPos, DictToken2Id


def download_transformer(cache: str) -> None:
    urls = ['https://raw.githubusercontent.com/openai/finetune-transformer-lm/master/model/params_{}.npy'.format(i) for
            i in range(10)]
    urls += ['https://raw.githubusercontent.com/openai/finetune-transformer-lm/master/model/params_shapes.json',
             'https://raw.githubusercontent.com/openai/finetune-transformer-lm/master/model/vocab_40000.bpe',
             'https://raw.githubusercontent.com/openai/finetune-transformer-lm/master/model/encoder_bpe_40000.json',
             'https://raw.githubusercontent.com/huggingface/pytorch-openai-transformer-lm/master/parameters_names.json']
    for url in urls:
        download(url, cache)


def _load_openai_pretrained_model(model, n_ctx=-1, n_special=-1, n_transfer=12,
                                  n_embd=768, vocab=40990, path='./cache/transformer'):
    download_transformer(path)
    names = json.load(open(os.path.join(path, 'parameters_names.json')))
    shapes = json.load(open(os.path.join(path, 'params_shapes.json')))
    offsets = np.cumsum([np.prod(shape) for shape in shapes])
    init_params = [np.load(os.path.join(path, 'params_{}.npy'.format(n))) for n in range(10)]
    init_params = np.split(np.concatenate(init_params, 0), offsets)[:-1]
    init_params = [param.reshape(shape) for param, shape in zip(init_params, shapes)]
    if n_ctx > 0:
        init_params[0] = init_params[0][:n_ctx]
    if n_special > 0:
        init_params[0] = np.concatenate(
            [init_params[1],
             (np.random.randn(n_special, n_embd) * 0.02).astype(np.float32),
             init_params[0]
             ], 0)
    else:
        init_params[0] = np.concatenate(
            [init_params[1],
             init_params[0]
             ], 0)
    del init_params[1]
    if n_transfer == -1:
        n_transfer = 0
    else:
        n_transfer = 1 + n_transfer * 12
    init_params = [arr.squeeze() for arr in init_params]

    try:
        assert (vocab, n_embd) == init_params[0].shape
    except AssertionError as e:
        e.args += ((vocab, n_embd), init_params[0].shape)
        raise

    embedding_weights = init_params[0]

    for name, ip in zip(names[1:n_transfer], init_params[1:n_transfer]):
        name = name[6:]  # skip "model/"
        assert name[-2:] == ":0"
        name = name[:-2]
        name = name.split('/')
        pointer = model
        for m_name in name:
            if re.fullmatch(r'[A-Za-z]+\d+', m_name):
                l = re.split(r'(\d+)', m_name)
            else:
                l = [m_name]
            pointer = getattr(pointer, l[0])
            if len(l) >= 2:
                num = int(l[1])
                pointer = pointer[num]
        try:
            assert pointer.shape == ip.shape
        except AssertionError as e:
            e.args += (pointer.shape, ip.shape)
            raise
        pointer.data = torch.from_numpy(ip)
    return embedding_weights


class Transformer(Model):
    """
    Transformer-LM:  Improving Language Understanding by Generative Pre-Training
    paper: https://s3-us-west-2.amazonaws.com/openai-assets/research-covers/language-unsupervised/language_understanding_paper.pdf
    tensorflow_code: https://github.com/openai/finetune-transformer-lm
    pytorch_code: https://github.com/huggingface/pytorch-openai-transformer-lm
    """

    def __init__(self, *, pooling_methods: Optional[Union[Pooling, List[Pooling]]] = None, causal: bool = True,
                 return_numpy: bool = True, batch_size: int = 64, cache: str = './cache/transformer') -> None:
        model = TransformerEncoder(resid_pdrop=0.1, n_embd=768, embd_pdrop=0.1, n_layer=12, n_head=12, attn_pdrop=0.1,
                                   n_ctx=512, causal=causal)
        embedding = _load_openai_pretrained_model(model, n_ctx=-1, n_special=-1, n_transfer=12, n_embd=768, vocab=40990,
                                                  path=cache)
        lookup = LookUp(embedding=EmbeddingWithPos(embedding[:40990 - 512], embedding[-512:]),
                        unk_handler_index=ConstantUnkHandler(0),
                        token2id=DictToken2Id(json.load(open(os.path.join(cache, 'encoder_bpe_40000.json')))))
        super().__init__(return_numpy=return_numpy, batch_size=batch_size, encoder=model,
                         normalizer=TransformerTokenizer(lower=True), lookups=lookup, pooling_methods=pooling_methods)
