from typing import Optional, List, Union
import torch.utils.model_zoo as model_zoo

from model import Model
from pooling.pooling import Pooling
from token_embedding.lookup import LookUp
from encoder.encoder import CoveBaseEncoder
from normalization.normalization import NamedTokenizer
from token_embedding.embedding_loader import GloveLoader
from token_embedding.unk_handler import ConstantUnkHandler
from token_embedding.embedding_loader import EmbeddingLoader
from token_embedding.embedding_builder import ReduceEmbedding

cove_model_urls = {'wmt-lstm': 'https://s3.amazonaws.com/research.metamind.io/cove/wmtlstm-8f474287.pth'}


class CoveEncoder(CoveBaseEncoder):
    def __init__(self, concat_intermediate: bool, concat_final: bool, concat_word_embedding: bool,
                 model_cache: str = './cache/cove', is_mock: bool = False) -> None:
        super().__init__(concat_intermediate, concat_final, concat_word_embedding, embedding_size=300)
        if is_mock:
            return
        state_dict = model_zoo.load_url(cove_model_urls['wmt-lstm'], model_dir=model_cache)
        if concat_intermediate:
            layer0_dict = {k: v for k, v in state_dict.items() if 'l0' in k}
            self.rnn0.load_state_dict(layer0_dict)
            if concat_final:
                layer1_dict = {k.replace('l1', 'l0'): v for k, v in state_dict.items() if 'l1' in k}
                self.rnn1.load_state_dict(layer1_dict)
        elif concat_final:
            self.rnn1.load_state_dict(model_zoo.load_url(cove_model_urls['wmt-lstm'], model_dir=model_cache))


class Cove(Model):
    """
    CoVe: Contextualized Word Vectors
    paper: http://papers.nips.cc/paper/7209-learned-in-translation-contextualized-word-vectors.pdf
    pytorch_code: https://github.com/salesforce/cove
    """

    def __init__(self, *, pooling_methods: Optional[Union[Pooling, List[Pooling]]] = None, return_numpy: bool = True,
                 batch_size: Optional[int] = 64, lower: bool = True, concat_intermediate: bool = False,
                 concat_final: bool = True, concat_word_embedding: bool = True,
                 glove_embedding: Optional[EmbeddingLoader] = None, use_moses: bool = True,
                 model_cache: str = './cache/cove', max_vocab_size: Optional[int] = None,
                 min_freq: Optional[int] = None, is_mock: bool = False):
        """
            NOTE: based on their paper we should use moses.pl script for
            tokenization but glove uses ptb (based on fastText's paper)
            or stanford (based on glove's paper)! :D
        """
        if batch_size is None:
            batch_size = 64
        encoder = CoveEncoder(concat_intermediate, concat_final, concat_word_embedding, model_cache, is_mock)
        normalizer = NamedTokenizer(lower, 'moses' if use_moses else 'ptb')
        if glove_embedding is None:
            glove_embedding = GloveLoader(name='mock' if is_mock else '840B', dim=300, cache='./cache/glove/',
                                          vocab_size=None, stoi=None)
        embedding = glove_embedding.embedding
        if max_vocab_size is None and min_freq is None:
            embedding_builder = None
        else:
            embedding_builder = ReduceEmbedding(base_embedding=embedding, base_token2id=glove_embedding.stoi,
                                                special_tokens=[glove_embedding.unk_token],
                                                max_vocab_size=max_vocab_size, min_freq=min_freq)
        lookup = LookUp(unk_handler_index=ConstantUnkHandler(glove_embedding.unk_index), embedding=embedding,
                        token2id=glove_embedding.stoi, embedding_builder=embedding_builder)
        super().__init__(return_numpy=return_numpy, batch_size=batch_size, normalizer=normalizer, encoder=encoder,
                         lookups=lookup, pooling_methods=pooling_methods)
