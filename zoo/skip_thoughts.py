import torch
import numpy as np
from collections import OrderedDict

from model import Model
from utils import download
from pooling.pooling import Last
from token_embedding.lookup import LookUp
from token_embedding.unk_handler import ConstantUnkHandler
from encoder.encoder import SkipThoughtsEncoder, ConcatEncoder
from token_embedding.embedding_loader import SkipThoughtsLoader
from normalization.normalization import NamedTokenizer, TextNormalizer

_skip_thoughts_urls = {
    'uni_skip': 'http://www.cs.toronto.edu/~rkiros/models/uni_skip.npz',
    'bi_skip': 'http://www.cs.toronto.edu/~rkiros/models/bi_skip.npz'
}


class PreTrainedSkipThoughtsEncoder(SkipThoughtsEncoder):
    def __init__(self, is_bidirectional: bool, cache: str = './cache/skipthoughts/') -> None:
        hidden_size = 1200 if is_bidirectional else 2400
        super().__init__(embedding_size=620, hidden_size=hidden_size, dropout=0.25, bidirectional=is_bidirectional)
        dict_ = np.load(
            download(url=_skip_thoughts_urls['bi_skip' if is_bidirectional else 'uni_skip'], target_dir=cache))
        params = OrderedDict()
        params['bias_ih_l0'] = torch.zeros(hidden_size * 3)
        params['bias_hh_l0'] = torch.zeros(hidden_size * 3)  # must stay equal to 0
        params['weight_ih_l0'] = torch.zeros(hidden_size * 3, 620)
        params['weight_hh_l0'] = torch.zeros(hidden_size * 3, hidden_size)
        params['weight_ih_l0'][:hidden_size * 2] = torch.from_numpy(dict_['encoder_W']).t()
        params['weight_ih_l0'][hidden_size * 2:] = torch.from_numpy(dict_['encoder_Wx']).t()
        params['bias_ih_l0'][:hidden_size * 2] = torch.from_numpy(dict_['encoder_b'])
        params['bias_ih_l0'][hidden_size * 2:] = torch.from_numpy(dict_['encoder_bx'])
        params['weight_hh_l0'][:hidden_size * 2] = torch.from_numpy(dict_['encoder_U']).t()
        params['weight_hh_l0'][hidden_size * 2:] = torch.from_numpy(dict_['encoder_Ux']).t()
        if is_bidirectional:
            params['bias_ih_l0_reverse'] = torch.zeros(hidden_size * 3)
            params['bias_hh_l0_reverse'] = torch.zeros(hidden_size * 3)  # must stay equal to 0
            params['weight_ih_l0_reverse'] = torch.zeros(hidden_size * 3, 620)
            params['weight_hh_l0_reverse'] = torch.zeros(hidden_size * 3, hidden_size)
            params['weight_ih_l0_reverse'][:hidden_size * 2] = torch.from_numpy(dict_['encoder_r_W']).t()
            params['weight_ih_l0_reverse'][hidden_size * 2:] = torch.from_numpy(dict_['encoder_r_Wx']).t()
            params['bias_ih_l0_reverse'][:hidden_size * 2] = torch.from_numpy(dict_['encoder_r_b'])
            params['bias_ih_l0_reverse'][hidden_size * 2:] = torch.from_numpy(dict_['encoder_r_bx'])
            params['weight_hh_l0_reverse'][:hidden_size * 2] = torch.from_numpy(dict_['encoder_r_U']).t()
            params['weight_hh_l0_reverse'][hidden_size * 2:] = torch.from_numpy(dict_['encoder_r_Ux']).t()
        self.rnn.load_state_dict(params)


class SkipThoughts(Model):
    """
    SkipThoughts: Skip-Thought Vectors
    paper: https://arxiv.org/abs/1506.06726
    theano_code: https://github.com/ryankiros/skip-thoughts
    pytorch_code: https://github.com/Cadene/skip-thoughts.torch
    """

    def __init__(self, *, return_numpy: bool = True, batch_size: int = 64, bidirectional: bool = False,
                 unidirectional: bool = False, cache: str = './cache/skipthoughts/'):
        assert bidirectional or unidirectional
        encoders = []
        if bidirectional:
            encoders.append(PreTrainedSkipThoughtsEncoder(True, cache))
        if unidirectional:
            encoders.append(PreTrainedSkipThoughtsEncoder(False, cache))
        encoder = ConcatEncoder(encoders)
        embedding = SkipThoughtsLoader(name='bi_skip' if bidirectional else 'uni_skip', dim=620, cache=cache)
        lookup = LookUp(embedding=embedding.embedding, token2id=embedding.stoi,
                        unk_handler_index=ConstantUnkHandler(embedding.unk_index))
        normalizer = TextNormalizer(tokenizer=NamedTokenizer(tokenizer_name='ptb', lower=True), postfix='<eos>')
        super().__init__(return_numpy=return_numpy, batch_size=batch_size, encoder=encoder, lookups=[lookup],
                         pooling_methods=Last(), normalizer=normalizer)
