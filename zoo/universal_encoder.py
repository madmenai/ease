import os
import numpy as np
import tensorflow as tf
from typing import List
import sentencepiece as spm
import tensorflow_hub as hub

from model import Model

tf.logging.set_verbosity(0)
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

_use_name_to_url = {
    'large-v3': 'https://tfhub.dev/google/universal-sentence-encoder-large/3',
    'large-v2': 'https://tfhub.dev/google/universal-sentence-encoder-large/2',
    'large-v1': 'https://tfhub.dev/google/universal-sentence-encoder-large/1',
    'normal-v2': 'https://tfhub.dev/google/universal-sentence-encoder/2',
    'normal-v1': 'https://tfhub.dev/google/universal-sentence-encoder/1',
    'lite-v2': 'https://tfhub.dev/google/universal-sentence-encoder-lite/2',
    'lite-v1': 'https://tfhub.dev/google/universal-sentence-encoder-lite/1'
}


class UniversalEncoder(Model):
    """
    USE: Universal Sentence Encoder
    paper: https://arxiv.org/abs/1803.11175
    tf_hub: https://tfhub.dev/google/universal-sentence-encoder/2
    """

    def __init__(self, batch_size: int = 64, encoder_name: str = 'large-v3'):
        assert encoder_name in _use_name_to_url
        super().__init__(return_numpy=True, batch_size=batch_size, encoder=None, normalizer=None,
                         lookups=None, pooling_methods=None)
        self.is_sentence_embedding = True
        module = hub.Module(_use_name_to_url[encoder_name])
        input_placeholder = tf.sparse_placeholder(tf.int64, shape=[None, None])
        self.encodings = module(inputs=dict(values=input_placeholder.values, indices=input_placeholder.indices,
                                            dense_shape=input_placeholder.dense_shape))
        self.input_placeholder = input_placeholder
        with tf.Session() as sess:
            spm_path = sess.run(module(signature='spm_path'))
        self.sp = spm.SentencePieceProcessor()
        self.sp.Load(spm_path)

    def process_to_IDs_in_sparse_format(self, sentences):
        ids = [self.sp.EncodeAsIds(x) for x in sentences]
        max_len = max(len(x) for x in ids)
        dense_shape = (len(ids), max_len)
        values = [item for sublist in ids for item in sublist]
        indices = [[row, col] for row in range(len(ids)) for col in range(len(ids[row]))]
        return values, indices, dense_shape

    def forward(self, sentences: List[str], is_training_data: bool = True) -> np.array:
        result = []
        with tf.Session() as session:
            session.run([tf.global_variables_initializer(), tf.tables_initializer()])
            for i in range(0, len(sentences), self.batch_size):
                values, indices, dense_shape = self.process_to_IDs_in_sparse_format(sentences[i:i + self.batch_size])
                outputs = session.run(self.encodings, feed_dict={self.input_placeholder.values: values,
                                                                 self.input_placeholder.indices: indices,
                                                                 self.input_placeholder.dense_shape: dense_shape})
                result.append(outputs)
        return np.concatenate(result, axis=0)
