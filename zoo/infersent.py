from typing import List, Optional, Union

from model import Model
from pooling.pooling import Pooling
from utils import download, torch_load
from encoder.encoder import LSTMEncoder
from token_embedding.lookup import LookUp
from token_embedding.unk_handler import ConstantUnkHandler
from token_embedding.embedding_builder import ReduceEmbedding
from normalization.normalization import TextNormalizer, NamedTokenizer
from token_embedding.embedding_loader import GloveLoader, EmbeddingLoader, FastTextLoader

_infer_sent_urls = 'https://s3.amazonaws.com/senteval/infersent/infersent{}.pkl'


class InferSent(Model):
    """
    InferSent: Supervised Learning of Universal Sentence Representations from Natural Language Inference Data
    paper: https://arxiv.org/abs/1705.02364
    pytorch_code: https://github.com/facebookresearch/InferSent
    """

    def __init__(self, *, pooling_methods: Optional[Union[Pooling, List[Pooling]]] = None, is_v1: bool = False,
                 lower: bool = True, cache: str = './cache/infersent/', return_numpy: bool = True, batch_size: int = 64,
                 embedding: Optional[EmbeddingLoader] = None, max_vocab_size: Optional[int] = None,
                 min_freq: Optional[int] = None, is_mock: bool = False):
        """
            NOTE: not sure about lower
        """
        encoder = LSTMEncoder(300, 2048, True, 1, 0)
        if not is_mock:
            encoder.load_state_dict(
                torch_load(download(_infer_sent_urls.format(1 if is_v1 else 2), cache)))
        if is_v1:
            prefix = '<s>'
            postfix = '</s>'
            tokenizer = 'ptb'
            if embedding is None:
                embedding = GloveLoader(name='840B' if not is_mock else 'mock', dim=300, cache='./cache/glove/',
                                        vocab_size=None, stoi=None)
        else:
            prefix = '<p>'
            postfix = '</p>'
            tokenizer = 'moses_hack'
            if embedding is None:
                embedding = FastTextLoader(name='crawl' if not is_mock else 'mock', cache='./cache/fasttext/',
                                           vocab_size=None, stoi=None)
        text_normalizer = TextNormalizer(prefix=prefix, postfix=postfix,
                                         tokenizer=NamedTokenizer(lower=lower, tokenizer_name=tokenizer))
        if max_vocab_size is None and min_freq is None:
            embedding_builder = None
        else:
            embedding_builder = ReduceEmbedding(base_embedding=embedding.embedding, base_token2id=embedding.stoi,
                                                special_tokens=[prefix, postfix, embedding.unk_token],
                                                max_vocab_size=max_vocab_size, min_freq=min_freq)
        lookup = LookUp(embedding=embedding.embedding, unk_handler_index=ConstantUnkHandler(embedding.unk_index),
                        token2id=embedding.stoi, embedding_builder=embedding_builder)
        super().__init__(return_numpy=return_numpy, batch_size=batch_size, encoder=encoder, normalizer=text_normalizer,
                         lookups=[lookup], pooling_methods=pooling_methods)
