import os
import json
import numpy as np
import tensorflow as tf
from model import Model
from utils import download
from zoo.qt import s2v_encoder
from encoder.encoder import Encoder

_qt_urls = {
    'umbc': {
        'data': '',
        'config_file': './zoo/qt/configs/umbc/eval.json',
    },
    'bc': {
        'data': '',
        'config_file': './zoo/qt/configs/bc/eval.json',
    },
    'glove-umbc': {
        'data': '',
        'config_file': './zoo/qt/configs/glove-umbc/eval.json',
    },
    'glove-bc': {
        'data': '',
        'config_file': './zoo/qt/configs/glove-bc/eval.json',
    },
    'dicts': ''
}


class QTConfig(object):
    """Wrapper for config params."""
    pass


class QuickThoughtsEncoder(Encoder):
    def __init__(self, name='umbc', cache='./cache/quick_thoughts/'):
        assert name in _qt_urls
        super().__init__(batch_first=True, input_is_packed=True, output_is_packed=True)
        self.cache = cache
        self.name = name
        self.encoders = []
        self.sessions = []
        self.model_folder = download(url=_qt_urls[name]['data'], target_dir=cache, decompress=True)
        self.dict_folder = download(url=_qt_urls['dicts'], target_dir=cache, decompress=True)
        self.dict_folder = os.path.join(self.cache, self.dict_folder + self.name)
        self.glove_folder = os.path.join(self.cache, 'dictionaries/glove')
        self.load_models()

    def load_models(self, mode='encode'):
        """Loads a configuration for a specific model."""
        config_dicts = json.load(open(_qt_urls[self.name]['config_file']))
        if isinstance(config_dicts, dict):
            config_dicts = [config_dicts]
        for config_dict in config_dicts:
            config = QTConfig()
            config.encoder = config_dict['encoder']
            config.encoder_dim = config_dict['encoder_dim']
            config.bidir = config_dict['bidir']
            config.uniform_init_scale = 0.1
            config.batch_size = 400
            config.use_norm = False
            config.sequence_length = 30
            if config_dict['checkpoint_path']:
                config.checkpoint_path = os.path.join(self.cache, config_dict['checkpoint_path'])
            config.vocab_configs = []
            for vocab_configs in config_dict['vocab_configs']:
                config.vocab_configs.append(self._vocab_config(vocab_configs, mode))
            self.load_model(config)

    def _vocab_config(self, vocab_config, mode):
        config = QTConfig()
        config.mode = vocab_config['mode']
        config.name = vocab_config['name']
        config.dim = vocab_config['dim']
        config.size = vocab_config['size']
        if config.mode == 'fixed' and mode != "eval":
            config.vocab_file = os.path.join(self.glove_folder, 'glove.840B.300d.txt')
            config.embs_file = os.path.join(self.glove_folder, 'glove.840B.300d.npy')
        elif mode == "encode" and config.mode == "trained":
            config.vocab_file = vocab_config['vocab_file']
        elif mode == "encode" and config.mode == "expand":
            config.vocab_file = os.path.join(self.cache, vocab_config['vocab_file'])
            config.embs_file = os.path.join(self.cache, vocab_config['embs_file'])
        return config

    def load_model(self, model_config):
        graph = tf.Graph()
        with graph.as_default():
            encoder = s2v_encoder.s2v_encoder(model_config)
            restore_model = encoder.build_graph_from_config(model_config)
        sess = tf.Session(graph=graph)
        restore_model(sess)
        self.encoders.append(encoder)
        self.sessions.append(sess)

    def encode(self,
               data,
               use_norm=True,
               verbose=False,
               batch_size=128,
               use_eos=False):
        """Encodes a sequence of sentences as skip-thought vectors.
        Args:
          data: A list of input strings.
          use_norm: If True, normalize output skip-thought vectors to unit L2 norm.
          verbose: Whether to log every batch.
          batch_size: Batch size for the RNN encoders.
          use_eos: If True, append the end-of-sentence word to each input sentence.
        Returns:
          thought_vectors: A list of numpy arrays corresponding to 'data'.
        Raises:
          ValueError: If called before calling load_encoder.
        """
        if not self.encoders:
            raise ValueError(
                "Must call load_model at least once before calling encode.")

        encoded = []
        for encoder, sess in zip(self.encoders, self.sessions):
            encoded.append(
                np.array(
                    encoder.encode(
                        sess,
                        data,
                        use_norm=use_norm,
                        verbose=verbose,
                        batch_size=batch_size,
                        use_eos=use_eos)))

        return np.concatenate(encoded, axis=1)

    def close(self):
        """Closes the active TensorFlow Sessions."""
        for sess in self.sessions:
            sess.close()

    def process_packed(self,
                       packed_inputs,
                       use_norm=True,
                       verbose=False,
                       batch_size=128,
                       use_eos=False):
        if not self.encoders:
            raise ValueError(
                "Must call load_model at least once before calling encode.")
        encoded = []
        for encoder, sess in zip(self.encoders, self.sessions):
            encoded.append(
                np.array(
                    encoder.encode(
                        sess,
                        packed_inputs,
                        use_norm=use_norm,
                        verbose=verbose,
                        batch_size=batch_size,
                        use_eos=use_eos)))
        return np.concatenate(encoded, axis=1)


class QuickThoughts(Model):
    def __init__(self, name: str, batch_size: int = 64):
        assert name in _qt_urls
        encoder = QuickThoughtsEncoder(name=name)
        super().__init__(return_numpy=True, batch_size=batch_size,  encoder=encoder,
                         normalizer=None, lookups=None, pooling_methods=None)
