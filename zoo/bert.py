import re
import os
import torch
import numpy as np
import tensorflow as tf
from typing import Union, Tuple, Optional, List

from model import Model
from utils import download, mkdir
from pooling.pooling import Pooling
from token_embedding.lookup import LookUp
from normalization.bert import BertTokenizer
from token_embedding.unk_handler import ConstantUnkHandler
from encoder.bert import BertConfig, BertModel, BertEncoder
from token_embedding.embedding import BertEmbedding, DictToken2Id

bert_urls = {
    'base_uncased': 'https://storage.googleapis.com/bert_models/2018_10_18/uncased_L-12_H-768_A-12.zip',
    'large_uncased': 'https://storage.googleapis.com/bert_models/2018_10_18/uncased_L-24_H-1024_A-16.zip',
    'base_cased': 'https://storage.googleapis.com/bert_models/2018_10_18/cased_L-12_H-768_A-12.zip',
    'base_multi': 'https://storage.googleapis.com/bert_models/2018_11_03/multilingual_L-12_H-768_A-12.zip',
    'base_chinese': 'https://storage.googleapis.com/bert_models/2018_11_03/chinese_L-12_H-768_A-12.zip',
    'multi_cased': 'https://storage.googleapis.com/bert_models/2018_11_23/multi_cased_L-12_H-768_A-12.zip'
}


def load_model(cache: str, bert_model_name: str) -> Tuple[BertModel, str]:
    mkdir(cache)
    path = download(bert_urls[bert_model_name], cache)
    config = BertConfig.from_json_file(os.path.join(path, 'bert_config.json'))
    model = BertModel(config)
    if os.path.exists(os.path.join(path, 'pytorch.model')):
        model.load_state_dict(torch.load(os.path.join(path, 'pytorch.model')))
        return model, path
    ckpt_path = os.path.join(path, 'bert_model.ckpt')
    init_vars = tf.train.list_variables(ckpt_path)
    names = []
    arrays = []
    for name, shape in init_vars:
        array = tf.train.load_variable(ckpt_path, name)
        names.append(name)
        arrays.append(array)
    for name, array in zip(names, arrays):
        name = name[5:]  # skip "bert/"
        name = name.split('/')
        if name[0] in ['redictions', 'eq_relationship']:
            continue
        pointer = model
        for m_name in name:
            if re.fullmatch(r'[A-Za-z]+_\d+', m_name):
                l = re.split(r'_(\d+)', m_name)
            else:
                l = [m_name]
            if l[0] == 'kernel':
                pointer = getattr(pointer, 'weight')
            else:
                pointer = getattr(pointer, l[0])
            if len(l) >= 2:
                num = int(l[1])
                pointer = pointer[num]
        if m_name[-11:] == '_embeddings':
            pointer = getattr(pointer, 'weight')
        elif m_name == 'kernel':
            array = np.transpose(array)
        try:
            assert pointer.shape == array.shape
        except AssertionError as e:
            e.args += (pointer.shape, array.shape)
            raise
        pointer.data = torch.from_numpy(array)
    torch.save(model.state_dict(), os.path.join(path, 'pytorch.model'))
    return model, path


class BERT(Model):
    """
    Bidirectional Encoder Representations from Transformers
    paper: https://arxiv.org/abs/1810.04805
    tensorflow_code: https://github.com/google-research/bert
    pytorch_code: https://github.com/huggingface/pytorch-pretrained-BERT
    """

    def __init__(self, *, model_name: str = 'base_uncased',
                 pooling_methods: Optional[Union[Pooling, List[Pooling]]] = None, return_numpy: bool = True,
                 batch_size: int = 64, model_cache: str = './cache/bert'):
        assert model_name in bert_urls.keys()
        lower = model_name.split('_')[1] != 'cased'
        model_name, path = load_model(model_cache, model_name)
        tokenizer = BertTokenizer(lower, os.path.join(path, 'vocab.txt'))
        lookup = LookUp(embedding=BertEmbedding(model_name),
                        unk_handler_index=ConstantUnkHandler(tokenizer.vocab['[PAD]']),
                        token2id=DictToken2Id(tokenizer.vocab))
        super().__init__(return_numpy=return_numpy, batch_size=batch_size, encoder=BertEncoder(model_name),
                         normalizer=tokenizer, lookups=lookup, pooling_methods=pooling_methods)
