import torch
from model import Model
from encoder.encoder import Encoder, DeepMojiEncoder
from utils import download
from pooling.pooling import Pooling
from token_embedding.lookup import LookUp
from typing import List, Optional, Union
from token_embedding.unk_handler import ConstantUnkHandler
from token_embedding.embedding_loader import DeepMojiLoader
from normalization.deepmoji import DeepMojiTokenizer
from zoo.torchmoji.model_def import load_specific_weights

deepmoji_urls = {
    'weights': 'https://www.dropbox.com/s/q8lax9ary32c7t9/pytorch_model.bin?dl=1'
}


class DeepMoji(Model):
    def __init__(self, *, return_numpy: bool = True, batch_size: int = 32, cache: str = './cache/deepmoji/',
                 pooling_methods: Optional[Union[Pooling, List[Pooling]]]) -> None:
        normalizer = DeepMojiTokenizer()
        deepmoji_embedding = DeepMojiLoader(dim=256, cache=cache)
        lookup = LookUp(unk_handler_index=ConstantUnkHandler(deepmoji_embedding.unk_index),
                        embedding=None, token2id=deepmoji_embedding.stoi)
        encoder = DeepMojiEncoder()
        weights_file = download(url=deepmoji_urls['weights'], target_dir=cache)
        load_specific_weights(encoder.model, weights_file, exclude_names=['output_layer'])
        super().__init__(return_numpy=return_numpy, batch_size=batch_size, normalizer=normalizer, lookups=lookup,
                         encoder=encoder, pooling_methods=pooling_methods)
