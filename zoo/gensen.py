import torch
from typing import Union, Optional, List

from model import Model
from pooling.pooling import Pooling
from token_embedding.lookup import LookUp
from encoder.encoder import BaseGenSenEncoder
from utils import download, load_pkl, torch_load
from token_embedding.embedding import Embedding, DictToken2Id
from normalization.normalization import TextNormalizer, NamedTokenizer
from token_embedding.embedding_loader import EmbeddingLoader, GloveLoader
from token_embedding.unk_handler import SkipThoughtUnkHandler, ConstantUnkHandler

_gen_sen_url = 'https://genseniclr2018.blob.core.windows.net/models/'


class GenSen(Model):
    """
    GenSen: Learning General Purpose Distributed Sentence Representations via Large Scale Multi-task Learning
    paper: https://openreview.net/forum?id=B18WgG-CZ&noteId=B18WgG-CZ
    pytorch_code: https://github.com/Maluuba/gensen
    """

    def __init__(self, *, pooling_methods: Optional[Union[Pooling, List[Pooling]]] = None,
                 model_name: str = 'nli_large', return_numpy: bool = True, batch_size: int = 64,
                 base_embedding: Optional[EmbeddingLoader] = None, cache: str = './cache/gensen/') -> None:
        assert model_name in {'nli_large_bothskip_2layer', 'nli_large_bothskip_parse',
                              'nli_large_bothskip', 'nli_large'}
        model_vocab = load_pkl(download(_gen_sen_url + model_name + '_vocab.pkl', cache), is_python2=True)
        encoder_model = torch_load(download(_gen_sen_url + model_name + '.model', cache))
        word2id = model_vocab['word2id']
        encoder = BaseGenSenEncoder(encoder_model['src_embedding.weight'].size(1),
                                    encoder_model['encoder.weight_hh_l0'].size(1), 1 if len(encoder_model) < 10 else 2)
        embedding_matrix = encoder_model['src_embedding.weight'].data.numpy()
        del encoder_model['src_embedding.weight']
        encoder.load_state_dict(encoder_model)
        if base_embedding is None:
            base_embedding = GloveLoader('840B', 300, './cache/glove/')
        skip_thought_unk_handler = SkipThoughtUnkHandler(embedding_matrix, word2id,
                                                         base_embedding.embedding.embedding.weight.data.cpu().numpy(),
                                                         base_embedding.stoi.dictionary)
        lookup = LookUp(embedding=Embedding(embedding_matrix, False), unk_handler_index=ConstantUnkHandler(0),
                        token2id=DictToken2Id(word2id), unk_handler_tensor=skip_thought_unk_handler)
        super().__init__(return_numpy=return_numpy, batch_size=batch_size, encoder=encoder,
                         normalizer=TextNormalizer(tokenizer=NamedTokenizer(lower=True, tokenizer_name='ptb'),
                                                   prefix='<s>', postfix='</s>'), lookups=lookup,
                         pooling_methods=pooling_methods)
