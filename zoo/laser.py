import torch
from typing import List, Optional, Union
from torch.nn.utils.rnn import PackedSequence

from model import Model
from utils import download, AddPath
from encoder.encoder import Encoder
from pooling.pooling import Pooling
from token_embedding.lookup import LookUp
from token_embedding.unk_handler import ConstantUnkHandler
from token_embedding.embedding_loader import LaserLoader, laser_urls


class LaserPretrainedEncoder(Encoder):
    def __init__(self, cache='./cache/laser/', encoder: Optional[torch.nn.Module] = None):
        super().__init__(batch_first=True, input_is_packed=True, output_is_packed=True)
        if encoder is None:
            network_file = download(laser_urls['network'], target_dir=cache)
            with AddPath('./zoo/laser'):
                network = torch.load(network_file)
            self.encoder = network.blstm
        else:
            self.encoder = encoder

    def process_packed(self, packed_inputs: PackedSequence) -> PackedSequence:
        return self.encoder.forward(self, packed_inputs)


class Laser(Model):
    def __init__(self, pooling_methods: Optional[Union[Pooling, List[Pooling]]],
                 return_numpy: bool = True, batch_size: int = 64, cache: str = './cache/laser'):
        embedding = LaserLoader(name='LASER', dim=384, cache=cache)
        encoder = LaserPretrainedEncoder(cache=cache, encoder=embedding.model.blstm)
        lookup = LookUp(embedding=embedding.embedding, token2id=embedding.stoi,
                        unk_handler_index=ConstantUnkHandler(embedding.unk_index))
        # TODO laser needs a normalizer: https://github.com/facebookresearch/LASER/tree/master/tools/normalize
        # TODO they have a postfix('</s>')
        # TODO add paper and code links
        super().__init__(return_numpy=return_numpy, batch_size=batch_size,
                         encoder=encoder, lookups=lookup, pooling_methods=pooling_methods, normalizer=normalizer)
