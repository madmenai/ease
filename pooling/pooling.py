import torch
from typing import List, Optional, Union
from distutils.version import LooseVersion


class Pooling:
    def __init__(self, *, ignore_lengths: bool) -> None:
        """
        given a sequence of (B, T, C) it will return (B, C`)

        :param ignore_lengths: if it's false then it will process each minibatch item individually based on its length
        """
        super().__init__()
        self.ignore_lengths = ignore_lengths

    def to(self, device: Union[str, torch.device]):
        return self

    def train(self, mode: bool = True):
        return self

    def process_all(self, x: torch.FloatTensor) -> torch.FloatTensor:
        """
        :param x: torch tensor with shape (B, T, C)
        :return: another tensor with shape (B, C`)
        """
        raise NotImplementedError()

    def process_single(self, x: torch.FloatTensor) -> torch.FloatTensor:
        """
        :param x: torch tensor with shape (T, C)
        :return: another tensor with shape (C`, )
        """
        raise NotImplementedError()

    def process(self, x: torch.FloatTensor, lengths: torch.LongTensor,
                tokenized_sentences: List[List[str]]) -> torch.FloatTensor:
        # NOTE we need tokenized_sentences for SIF-like pooling methods
        if self.ignore_lengths:
            return self.process_all(x)
        else:
            return torch.stack([self.process_single(x[i, :lengths[i]]) for i in range(x.shape[0])], dim=0)

    def __call__(self, x: torch.FloatTensor, lengths: torch.LongTensor,
                 tokenized_sentences: List[List[str]]) -> torch.FloatTensor:
        return self.process(x, lengths, tokenized_sentences)


class Average(Pooling):
    def __init__(self):
        super().__init__(ignore_lengths=False)

    def process_single(self, x: torch.FloatTensor) -> torch.FloatTensor:
        return torch.mean(x, dim=0)


class Max(Pooling):
    def __init__(self):
        super().__init__(ignore_lengths=False)

    def process_single(self, x: torch.FloatTensor) -> torch.FloatTensor:
        return torch.max(x, dim=0)


class Min(Pooling):
    def __init__(self):
        super().__init__(ignore_lengths=False)

    def process_single(self, x: torch.FloatTensor) -> torch.FloatTensor:
        return torch.min(x, dim=0)


class Last(Pooling):
    def __init__(self):
        super().__init__(ignore_lengths=False)

    def process_single(self, x: torch.FloatTensor) -> torch.FloatTensor:
        return x[-1]


class First(Pooling):
    def __init__(self):
        super().__init__(ignore_lengths=True)

    def process_all(self, x: torch.FloatTensor) -> torch.FloatTensor:
        return x[0]


class PNorm(Pooling):
    def __init__(self, power_values: Optional[List[float]] = None):
        super().__init__(ignore_lengths=False)
        assert len(power_values) > 0
        if power_values is None:
            power_values = [1, -1, 0, float('+inf'), float('-inf')]
        for power_value in power_values:
            if power_value == float('-inf') and LooseVersion(torch.__version__) < LooseVersion('1.0'):
                assert False, 'power values can not contain -inf in old versions of pytorch'
        self.power_values = power_values

    def process_single(self, x: torch.FloatTensor) -> torch.FloatTensor:
        return torch.cat([torch.norm(x, p, dim=0) for p in self.power_values], dim=0)
