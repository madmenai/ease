import torch
import random
import unittest
import numpy as np
from typing import List, Any, Callable


class UnitTest(unittest.TestCase):
    def __init__(self, method_name='runTest'):
        super().__init__(method_name)

    def setUp(self):
        seed = 12334
        random.seed(seed)
        np.random.seed(seed)
        torch.manual_seed(seed)
        if torch.cuda.is_available():
            torch.cuda.manual_seed(seed)

    @staticmethod
    def check_list_eq(l1: List[Any], l2: List[Any]) -> None:
        assert len(l1) == len(l2)
        for e1, e2 in zip(l1, l2):
            assert e1 == e2

    @staticmethod
    def check_all_elements(l: List[Any], func: Callable[[Any], bool], recursive: bool = True) -> None:
        for e in l:
            if recursive and isinstance(e, (list, tuple)):
                UnitTest.check_all_elements(e, func)
            else:
                assert func(e)
