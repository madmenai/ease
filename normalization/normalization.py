import spacy
from polyglot.text import Text
from typing import List, Callable
from sacremoses import MosesTokenizer
from nltk.tokenize import word_tokenize
from nltk.tokenize.toktok import ToktokTokenizer

_toktok = ToktokTokenizer()
_spacy_en = spacy.load('en', disable=['parser', 'tagger', 'ner', 'textcat'])
_moses_tokenizer = MosesTokenizer()


def _none_tokenizer(s: str) -> List[str]:
    return [s]


def _split_tokenizer(s: str) -> List[str]:
    return s.split()


def _spacy_tokenizer(s: str) -> List[str]:
    return [tok.text for tok in _spacy_en.tokenizer(s)]


def _polyglot_tokenizer(s: str) -> List[str]:
    return Text(s).tokens


def _moses_hack_tokenizer(s: str) -> List[str]:
    s = ' '.join(word_tokenize(s))
    s = s.replace(" n't ", "n 't ")  # HACK to get ~MOSES tokenization
    return s.split()


def _fetch_tokenizer(name: str) -> Callable[[str], List[str]]:
    if name == 'space':
        return _split_tokenizer
    if name == 'spacy':
        return _spacy_tokenizer
    if name == 'moses':
        return _moses_tokenizer.tokenize
    if name == 'moses_hack':
        return _moses_hack_tokenizer
    if name == 'ptb':
        return word_tokenize
    if name == 'toktok':
        return _toktok.tokenize
    if name == 'none':
        return _none_tokenizer
    if name == 'polyglot':
        return _polyglot_tokenizer
    raise ValueError()


class Tokenizer:
    def __init__(self, *, lower: bool) -> None:
        super().__init__()
        self.lower = lower

    def __call__(self, sentence: str) -> List[str]:
        return self.process(sentence.lower() if self.lower else sentence)

    def process(self, sentence: str) -> List[str]:
        raise NotImplementedError()


class NamedTokenizer(Tokenizer):
    def __init__(self, lower: bool, tokenizer_name: str) -> None:
        super().__init__(lower=lower)
        self.tokenizer = _fetch_tokenizer(tokenizer_name)

    def process(self, sentence: str) -> List[str]:
        return self.tokenizer(sentence)


class TextNormalizer:
    def __init__(self, *, tokenizer: Tokenizer, prefix: str = '', postfix: str = '') -> None:
        self.tokenizer = tokenizer
        self.prefix = [] if prefix == '' else [prefix]
        self.postfix = [] if postfix == '' else [postfix]

    def __call__(self, sentences: List[str]) -> List[List[str]]:
        preprocessed_sentences = [s.strip() for s in sentences]
        return [self.prefix + self.tokenizer(s) + self.postfix for s in preprocessed_sentences]
