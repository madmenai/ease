from typing import List

from normalization.normalization import Tokenizer
from zoo.torchmoji.word_generator import WordGenerator


class DeepMojiTokenizer(Tokenizer):
    def __init__(self, *, lower: bool = False) -> None:
        super().__init__(lower=lower)
        self.word_gen = WordGenerator(stream=None, allow_unicode_text=True, ignore_emojis=False,
                                      remove_variation_selectors=True, break_replacement=True)

    def process(self, sentence: str) -> List[str]:
        return self.word_gen.get_words(sentence)
