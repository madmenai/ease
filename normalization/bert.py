import collections
import unicodedata
from typing import List, Union

from normalization.normalization import Tokenizer


class BertTokenizer(Tokenizer):
    def __init__(self, lower: bool = True, vocab_file: str = './cache/bert_multilingual.vocab'):
        super().__init__(lower=False)  # we are doing our custom lower
        self.vocab = self.load_vocab(vocab_file)
        self.lower = lower
        self.max_input_chars_per_word = 100
        self.unk_token = '[UNK]'

    @staticmethod
    def load_vocab(vocab_file: str):
        vocab = collections.OrderedDict()
        index = 0
        with open(vocab_file) as reader:
            while True:
                token = BertTokenizer.convert_to_unicode(reader.readline())
                if not token:
                    break
                token = token.strip()
                vocab[token] = index
                index += 1
        return vocab

    @staticmethod
    def convert_to_unicode(text: Union[str, bytes]):
        if isinstance(text, str):
            return text
        elif isinstance(text, bytes):
            return text.decode('utf-8', 'ignore')
        else:
            raise ValueError('Unsupported string type: {}'.format(type(text)))

    def tokenize(self, text: str):
        text = self.convert_to_unicode(text)
        text = self._clean_text(text)
        text = self._tokenize_chinese_chars(text)
        orig_tokens = self.whitespace_tokenize(text)
        split_tokens = []
        for token in orig_tokens:
            if self.lower:
                token = token.lower()
                token = self._run_strip_accents(token)
            split_tokens.extend(self._run_split_on_punc(token))
        output_tokens = self.whitespace_tokenize(' '.join(split_tokens))
        return output_tokens

    @staticmethod
    def whitespace_tokenize(text):
        text = text.strip()
        if not text:
            return []
        return text.split()

    @staticmethod
    def _run_strip_accents(text):
        text = unicodedata.normalize('NFD', text)
        output = []
        for char in text:
            cat = unicodedata.category(char)
            if cat == 'Mn':
                continue
            output.append(char)
        return ''.join(output)

    def _run_split_on_punc(self, text):
        chars = list(text)
        i = 0
        start_new_word = True
        output = []
        while i < len(chars):
            char = chars[i]
            if self._is_punctuation(char):
                output.append([char])
                start_new_word = True
            else:
                if start_new_word:
                    output.append([])
                start_new_word = False
                output[-1].append(char)
            i += 1
        return [''.join(x) for x in output]

    def _tokenize_chinese_chars(self, text):
        output = []
        for char in text:
            cp = ord(char)
            if self._is_chinese_char(cp):
                output.append(' ')
                output.append(char)
                output.append(' ')
            else:
                output.append(char)
        return ''.join(output)

    @staticmethod
    def _is_chinese_char(cp):
        return (0x4E00 <= cp <= 0x9FFF) or (0x3400 <= cp <= 0x4DBF) or (0x20000 <= cp <= 0x2A6DF) or (
                0x2A700 <= cp <= 0x2B73F) or (0x2B740 <= cp <= 0x2B81F) or (0x2B820 <= cp <= 0x2CEAF) or (
                       0xF900 <= cp <= 0xFAFF) or (0x2F800 <= cp <= 0x2FA1F)

    def _clean_text(self, text):
        """Performs invalid character removal and whitespace cleanup on text."""
        output = []
        for char in text:
            cp = ord(char)
            if cp == 0 or cp == 0xfffd or self._is_control(char):
                continue
            if self._is_whitespace(char):
                output.append(' ')
            else:
                output.append(char)
        return ''.join(output)

    def process(self, sentence: str) -> List[str]:
        split_tokens = []
        for token in self.tokenize(sentence):
            for sub_token in self.wordpiece_tokenize(token):
                split_tokens.append(sub_token)
        return split_tokens

    def wordpiece_tokenize(self, text):
        text = self.convert_to_unicode(text)

        output_tokens = []
        for token in self.whitespace_tokenize(text):
            chars = list(token)
            if len(chars) > self.max_input_chars_per_word:
                output_tokens.append(self.unk_token)
                continue

            is_bad = False
            start = 0
            sub_tokens = []
            while start < len(chars):
                end = len(chars)
                cur_substr = None
                while start < end:
                    substr = ''.join(chars[start:end])
                    if start > 0:
                        substr = '##' + substr
                    if substr in self.vocab:
                        cur_substr = substr
                        break
                    end -= 1
                if cur_substr is None:
                    is_bad = True
                    break
                sub_tokens.append(cur_substr)
                start = end

            if is_bad:
                output_tokens.append(self.unk_token)
            else:
                output_tokens.extend(sub_tokens)
        return output_tokens

    @staticmethod
    def _is_whitespace(char):
        if char == ' ' or char == '\t' or char == '\n' or char == '\r':
            return True
        cat = unicodedata.category(char)
        if cat == 'Zs':
            return True
        return False

    @staticmethod
    def _is_control(char):
        if char == '\t' or char == '\n' or char == '\r':
            return False
        cat = unicodedata.category(char)
        if cat.startswith('C'):
            return True
        return False

    @staticmethod
    def _is_punctuation(char):
        cp = ord(char)
        # We treat all non-letter/number ASCII as punctuation.
        # Characters such as "^", "$", and "`" are not in the Unicode
        # Punctuation class but we treat them as punctuation anyways, for consistency.
        if (33 <= cp <= 47) or (58 <= cp <= 64) or (91 <= cp <= 96) or (123 <= cp <= 126):
            return True
        cat = unicodedata.category(char)
        if cat.startswith('P'):
            return True
        return False
