import torch
import torch.nn as nn
from typing import List
from torch.nn.utils.rnn import PackedSequence
from torch.nn.utils.rnn import pack_padded_sequence as pack
from torch.nn.utils.rnn import pad_packed_sequence as unpack

from utils import MaybePacked, is_packed
from zoo.torchmoji.model_def import TorchMoji
from zoo.torchmoji.global_variables import NB_TOKENS


class Encoder(nn.Module):
    def __init__(self, *, batch_first: bool, input_is_packed: bool, output_is_packed: bool,
                 returns_pooled: bool = False) -> None:
        super().__init__()
        self.batch_first = batch_first  # NOTE both input and output should be in this format
        self.input_should_be_packed = input_is_packed
        self.output_is_packed = output_is_packed
        self.returns_pooled = returns_pooled

    def __call__(self, inputs: MaybePacked, lengths: torch.LongTensor, input_is_batch_first: bool) -> MaybePacked:
        if self.batch_first != input_is_batch_first:
            if is_packed(inputs):
                inputs = unpack(inputs, batch_first=input_is_batch_first)[0]
            inputs = inputs.permute(1, 0, 2)
        if self.input_should_be_packed:
            if not is_packed(inputs):
                inputs = pack(inputs, lengths, batch_first=self.batch_first)
            inputs = self.process_packed(inputs)
        else:
            if is_packed(inputs):
                inputs = unpack(inputs, batch_first=self.batch_first)[0]
            inputs = self.process(inputs, lengths)
        return inputs

    def process(self, inputs: torch.FloatTensor, lengths: torch.LongTensor) -> MaybePacked:
        raise NotImplementedError()

    def process_packed(self, packed_inputs: PackedSequence) -> MaybePacked:
        raise NotImplementedError()


class ConcatEncoder(Encoder):
    def __init__(self, encoders: List[Encoder]) -> None:
        assert len(encoders) > 0
        returns_pooled = encoders[0].returns_pooled
        batch_first = {'t': 0, 'f': 0}
        input_is_packed = {'t': 0, 'f': 0}
        output_is_packed = {'t': 0, 'f': 0}
        for encoder in encoders:
            assert encoder.returns_pooled == returns_pooled
            batch_first['t' if encoder.batch_first else 'f'] += 1
            input_is_packed['t' if encoder.input_is_packed else 'f'] += 1
            output_is_packed['t' if encoder.output_is_packed else 'f'] += 1
        super().__init__(batch_first=batch_first['t'] >= batch_first['f'],
                         input_is_packed=input_is_packed['t'] > input_is_packed['f'],
                         output_is_packed=output_is_packed['t'] > output_is_packed['f'],
                         returns_pooled=returns_pooled)
        self.encoders = nn.ModuleList(encoders)

    def __call__(self, inputs: MaybePacked, lengths: torch.LongTensor, input_is_batch_first: bool) -> MaybePacked:
        outputs = [encoder(inputs, lengths, input_is_batch_first) for encoder in self.encoders]
        for i, encoder in enumerate(self.encoders):
            if self.batch_first != encoder.batch_first:
                if is_packed(outputs[i]):
                    outputs[i] = unpack(outputs[i], batch_first=encoder.batch_first)[0]
                outputs[i] = outputs[i].permute(1, 0, 2)
            if self.output_is_packed and not is_packed(outputs[i]):
                outputs[i] = pack(outputs[i], lengths, batch_first=self.batch_first)
            if not self.output_is_packed and is_packed(outputs[i]):
                outputs[i] = unpack(outputs[i], batch_first=self.batch_first)[0]
        if self.output_is_packed:
            return PackedSequence(torch.cat([output.data for output in outputs], 1), outputs[0].batch_sizes)
        return torch.cat(outputs, 2)


class SeriesEncoder(Encoder):
    def __init__(self, encoders: List[Encoder]) -> None:
        assert len(encoders) > 0
        for i, encoder in enumerate(encoders):
            assert not encoder.returns_pooled or i == (len(encoders) - 1)
        super().__init__(batch_first=encoders[0].batch_first, input_is_packed=encoders[0].input_should_be_packed,
                         output_is_packed=encoders[-1].output_is_packed, returns_pooled=encoders[-1].returns_pooled)
        self.encoders = nn.ModuleList(encoders)

    def __call__(self, inputs: MaybePacked, lengths: torch.LongTensor, input_is_batch_first: bool) -> MaybePacked:
        outputs = inputs
        output_is_batch_first = input_is_batch_first
        for encoder in self.encoders:
            outputs = encoder(outputs, lengths, output_is_batch_first)
            output_is_batch_first = encoder.batch_first
        return outputs


class LSTMEncoder(Encoder):
    def __init__(self, embedding_size: int, hidden_size: int, bidirectional: bool, num_layers: int,
                 drop_out: float):
        super().__init__(batch_first=False, input_is_packed=True, output_is_packed=True)
        if num_layers == 1:
            drop_out = 0
        self.enc_lstm = nn.LSTM(input_size=embedding_size, hidden_size=hidden_size, num_layers=num_layers,
                                bidirectional=bidirectional, dropout=drop_out)

    def process_packed(self, packed_inputs: PackedSequence) -> PackedSequence:
        return self.enc_lstm(packed_inputs)[0]


class BaseGenSenEncoder(Encoder):
    def __init__(self, embedding_size: int, hidden_size: int, num_layers: int):
        super().__init__(batch_first=True, input_is_packed=True, output_is_packed=True)
        self.encoder = nn.GRU(input_size=embedding_size, hidden_size=hidden_size,
                              num_layers=num_layers, batch_first=True, bidirectional=True)

    def process_packed(self, packed_inputs: PackedSequence) -> PackedSequence:
        return self.encoder(packed_inputs)[0]


class CoveBaseEncoder(Encoder):

    def __init__(self, concat_intermediate: bool, concat_final: bool, concat_word_embedding: bool,
                 embedding_size: int = 300) -> None:
        super().__init__(batch_first=True, input_is_packed=True, output_is_packed=True)
        self.concat_intermediate = concat_intermediate
        self.concat_final = concat_final
        self.concat_glove = concat_word_embedding
        if concat_intermediate:
            self.rnn0 = nn.LSTM(embedding_size, embedding_size, num_layers=1, bidirectional=True, batch_first=True)
            if concat_final:
                self.rnn1 = nn.LSTM(2 * embedding_size, embedding_size, num_layers=1, bidirectional=True,
                                    batch_first=True)
        elif concat_final:
            self.rnn1 = nn.LSTM(embedding_size, embedding_size, num_layers=2, bidirectional=True, batch_first=True)
        else:
            raise ValueError('At least one of layer0 and layer1 must be True.')

    def process_packed(self, packed_inputs: PackedSequence) -> PackedSequence:
        outputs = [packed_inputs.data] if self.concat_glove else []
        batch_sizes = packed_inputs.batch_sizes
        if self.concat_intermediate:
            outputs0, hidden_t0 = self.rnn0(packed_inputs, None)
            outputs.append(outputs0.data)
            packed_inputs = outputs0
        if self.concat_final:
            outputs1, hidden_t1 = self.rnn1(packed_inputs, None)
            outputs.append(outputs1.data)
        return PackedSequence(torch.cat(outputs, 1), batch_sizes)


class SkipThoughtsEncoder(Encoder):
    def __init__(self, embedding_size: int, hidden_size: int, bidirectional: bool, dropout: float):
        super().__init__(batch_first=True, input_is_packed=True, output_is_packed=True)
        self.rnn = nn.GRU(input_size=embedding_size, hidden_size=hidden_size, batch_first=True, dropout=dropout,
                          bidirectional=bidirectional)

    def process_packed(self, packed_inputs: PackedSequence) -> PackedSequence:
        return self.rnn(packed_inputs)[0]


class DeepMojiEncoder(Encoder):
    def __init__(self) -> None:
        super().__init__(batch_first=True, input_is_packed=True, output_is_packed=True)
        self.model = TorchMoji(nb_classes=None, nb_tokens=NB_TOKENS, feature_output=True, return_attention=False)

    def process_packed(self, packed_inputs: PackedSequence) -> MaybePacked:
        return self.model(packed_inputs)
