# borrowed from https://github.com/huggingface/pytorch-openai-transformer-lm
import copy
import math
import torch
import torch.nn as nn
from torch.nn.parameter import Parameter

from encoder.encoder import Encoder


def gelu(x):
    return 0.5 * x * (1 + torch.tanh(math.sqrt(2 / math.pi) * (x + 0.044715 * torch.pow(x, 3))))


class LayerNorm(nn.Module):
    "Construct a layernorm module in the OpenAI style (epsilon inside the square root)."

    def __init__(self, n_state, e=1e-5):
        super().__init__()
        self.g = nn.Parameter(torch.ones(n_state))
        self.b = nn.Parameter(torch.zeros(n_state))
        self.e = e

    def forward(self, x):
        u = x.mean(-1, keepdim=True)
        s = (x - u).pow(2).mean(-1, keepdim=True)
        x = (x - u) / torch.sqrt(s + self.e)
        return self.g * x + self.b


class Conv1D(nn.Module):
    def __init__(self, nf, nx):
        super().__init__()
        self.nf = nf
        w = torch.empty(nx, nf)
        nn.init.normal_(w, std=0.02)
        self.w = Parameter(w)
        self.b = Parameter(torch.zeros(nf))

    def forward(self, x):
        size_out = x.size()[:-1] + (self.nf,)
        x = torch.addmm(self.b, x.contiguous().view(-1, x.size(-1)), self.w)
        return x.view(*size_out)


class Attention(nn.Module):
    def __init__(self, n_state, n_ctx, attn_pdrop, resid_pdrop, n_head, causal):
        super().__init__()
        assert n_state % n_head == 0
        if causal:
            self.register_buffer('b', torch.tril(torch.ones(n_ctx, n_ctx)).view(1, 1, n_ctx, n_ctx))
            self.register_buffer('bc', -1e9 * (1 - torch.tril(torch.ones(n_ctx, n_ctx)).view(1, 1, n_ctx, n_ctx)))
        self.causal = causal
        self.n_head = n_head
        self.split_size = n_state
        self.c_attn = Conv1D(n_state * 3, n_state)
        self.c_proj = Conv1D(n_state, n_state)
        self.attn_dropout = nn.Dropout(attn_pdrop)
        self.resid_dropout = nn.Dropout(resid_pdrop)

    def _attn(self, q, k, v):
        w = torch.matmul(q, k)
        w = w / math.sqrt(v.size(-1))
        if self.causal:
            ctx = w.size(2)
            w = w * self.b[:, :, :ctx, :ctx] + self.bc[:, :, :ctx, :ctx]
        w = nn.Softmax(dim=-1)(w)
        w = self.attn_dropout(w)
        return torch.matmul(w, v)

    @staticmethod
    def merge_heads(x):
        x = x.permute(0, 2, 1, 3).contiguous()
        new_x_shape = x.size()[:-2] + (x.size(-2) * x.size(-1),)
        return x.view(*new_x_shape)

    def split_heads(self, x, k=False):
        new_x_shape = x.size()[:-1] + (self.n_head, x.size(-1) // self.n_head)
        x = x.view(*new_x_shape)
        if k:
            return x.permute(0, 2, 3, 1)
        else:
            return x.permute(0, 2, 1, 3)

    def forward(self, x):
        x = self.c_attn(x)
        query, key, value = x.split(self.split_size, dim=2)
        query = self.split_heads(query)
        key = self.split_heads(key, k=True)
        value = self.split_heads(value)
        a = self._attn(query, key, value)
        a = self.merge_heads(a)
        a = self.c_proj(a)
        a = self.resid_dropout(a)
        return a


class MLP(nn.Module):
    def __init__(self, n_state, n_embd, resid_pdrop):
        super().__init__()
        self.c_fc = Conv1D(n_state, n_embd)
        self.c_proj = Conv1D(n_embd, n_state)
        self.dropout = nn.Dropout(resid_pdrop)

    def forward(self, x):
        h = gelu(self.c_fc(x))
        h2 = self.c_proj(h)
        return self.dropout(h2)


class Block(nn.Module):
    def __init__(self, n_ctx, resid_pdrop, n_embd, n_head, attn_pdrop, causal):
        super().__init__()
        self.attn = Attention(n_embd, n_ctx, attn_pdrop, resid_pdrop, n_head, causal)
        self.ln_1 = LayerNorm(n_embd)
        self.mlp = MLP(4 * n_embd, n_embd, resid_pdrop)
        self.ln_2 = LayerNorm(n_embd)

    def forward(self, x):
        a = self.attn(x)
        n = self.ln_1(x + a)
        m = self.mlp(n)
        h = self.ln_2(n + m)
        return h


class TransformerEncoder(Encoder):
    """ Transformer model """

    def __init__(self, resid_pdrop, n_embd, embd_pdrop, n_layer, n_head,
                 attn_pdrop, n_ctx=512, causal=True):
        super().__init__(batch_first=True, input_is_packed=False, output_is_packed=False)
        self.n_embd = n_embd
        self.drop = nn.Dropout(embd_pdrop)
        block = Block(n_ctx, resid_pdrop, n_embd, n_head, attn_pdrop, causal)
        self.h = nn.ModuleList([copy.deepcopy(block) for _ in range(n_layer)])

    def process(self, x: torch.FloatTensor, lengths: torch.LongTensor) -> torch.FloatTensor:
        h = x
        for block in self.h:
            h = block(h)
        return h
