import torch
import numpy as np
import torch.nn as nn
from typing import List, Union, Tuple, Optional
from torch.nn.utils.rnn import pad_packed_sequence as unpack

from encoder.encoder import Encoder
from pooling.pooling import Pooling
from token_embedding.lookup import LookUp
from utils import listify, is_packed, set_device
from normalization.normalization import TextNormalizer, Tokenizer


class Model(nn.Module):
    def __init__(self, *, return_numpy: bool, batch_size: int, normalizer: Union[TextNormalizer, Tokenizer],
                 lookups: Union[LookUp, List[LookUp]], encoder: Optional[Encoder],
                 pooling_methods: Optional[Union[Pooling, List[Pooling]]]) -> None:
        """
        class representing a full sentence/token embedding with all the pre and post processing

        :param return_numpy: whether to return numpy arrays or pytorch tensors(only applicable to pytorch models)
        :param batch_size: number of sentences to use per minibatch
        :param normalizer: :class:`TextNormalizer` to use for text normalization and tokenization
        :param lookups: list of :class:`LookUp`s to use for word embedding, they will be concatenated
        :param encoder: :class:`Encoder` to use for embedding
        :param pooling_methods: list of :class:`Pooling`s to use after embedding, they will be concatenated;
                        if no pooling is provided then the output will be contextualized token embeddings
        """
        super().__init__()
        self.device = set_device('cpu')
        self.trainable = False
        self.batch_size = batch_size
        self.return_numpy = return_numpy
        self.normalizer = TextNormalizer(tokenizer=normalizer) if isinstance(normalizer, Tokenizer) else normalizer
        self.lookups = listify(lookups)
        self.encoder = encoder
        self.pooling_methods = listify(pooling_methods)
        self.is_sentence_embedding = len(self.pooling_methods) > 0
        if self.encoder is not None:
            if self.encoder.returns_pooled:
                assert len(self.pooling_methods) == 0
            self.is_sentence_embedding = self.encoder.returns_pooled or self.is_sentence_embedding

    def train(self, mode: bool = True):
        if self.return_numpy and mode:
            mode = False
            print('Can not be trainable and return numpy at the same time, setting trainable to false')
        self.trainable = mode
        for lookup in self.lookups:
            lookup.train(mode)
        if self.encoder is not None:
            self.encoder.train(mode)
        for pooling_method in self.pooling_methods:
            pooling_method.train(mode)
        return super().train(mode)

    def to(self, device: Union[str, torch.device]):
        device = set_device(device)
        self.device = device
        for lookup in self.lookups:
            lookup.to(device)
        if self.encoder is not None:
            self.encoder.to(device)
        for pooling_method in self.pooling_methods:
            pooling_method.to(device)
        return super().to(device)

    def _prepare(self, sentences: List[str], is_training_data: bool) -> List[List[str]]:
        sentences = self.normalizer(sentences)
        if is_training_data:
            for lookup in self.lookups:
                lookup.pre_process(sentences)
        return sentences

    def _lookup_batch(self, preprocessed_sentences: List[List[str]]) \
            -> Tuple[torch.FloatTensor, torch.LongTensor, torch.LongTensor, torch.LongTensor]:
        padded_token_embeddings = []
        lengths = None
        for lookup in self.lookups:
            padded_token_embedding, _lengths = lookup(preprocessed_sentences)
            padded_token_embeddings.append(padded_token_embedding)
            if lengths is None:
                lengths = _lengths
            assert (lengths == _lengths).all()
        padded_token_embeddings = torch.cat(padded_token_embeddings, dim=-1)  # NOTE this is a batch_first thing
        lengths, indices = torch.sort(lengths, 0, True)
        _indices = torch.sort(indices, 0)[1]  # for undoing the sort
        outputs = padded_token_embeddings[indices]  # sort
        return outputs, lengths, indices, _indices

    def _encode_batch(self, outputs: torch.FloatTensor, lengths: torch.LongTensor) -> torch.FloatTensor:
        if self.encoder is not None:
            outputs = self.encoder(outputs, lengths, input_is_batch_first=True)
            output_is_batch_first = self.encoder.batch_first
            if not self.encoder.returns_pooled:
                if is_packed(outputs):
                    outputs = unpack(outputs, batch_first=output_is_batch_first)[0]
                if not output_is_batch_first:
                    outputs = outputs.permute(1, 0, 2)
        return outputs  # NOTE this is always batch_first and unpacked

    def _process_batch(self, preprocessed_sentences: List[List[str]]) -> List[torch.FloatTensor]:
        outputs, lengths, indices, _indices = self._lookup_batch(preprocessed_sentences)
        outputs = self._encode_batch(outputs, lengths)
        if len(self.pooling_methods) > 0:
            tokenized_sentences = [preprocessed_sentences[i] for i in indices.tolist()]
            outputs = torch.cat([p.process(outputs, lengths, tokenized_sentences) for p in self.pooling_methods], dim=1)
        outputs = outputs[_indices]
        if self.is_sentence_embedding:
            return [outputs]
        lengths = lengths[_indices]
        return [outputs[i, :lengths[i]] for i in range(outputs.shape[0])]

    def forward(self, sentences: List[str], is_training_data: bool = True) \
            -> Union[torch.FloatTensor, np.array, Union[List[torch.FloatTensor], List[np.array]]]:
        sentences = self._prepare(sentences, is_training_data)
        result_embeddings = []
        for i in range(0, len(sentences), self.batch_size):
            preprocessed_sentences = sentences[i:i + self.batch_size]
            outputs = self._process_batch(preprocessed_sentences)
            if not self.trainable or self.return_numpy:
                outputs = [o.detach() for o in outputs]
            if self.return_numpy:
                outputs = [o.cpu().numpy() for o in outputs]
            result_embeddings.extend(outputs)
        if self.is_sentence_embedding:
            if self.return_numpy:
                return np.concatenate(result_embeddings, axis=0)
            return torch.cat(result_embeddings, dim=0)
        else:
            return result_embeddings
