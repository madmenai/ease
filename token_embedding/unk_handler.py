import torch
import numpy as np
from sklearn.linear_model import LinearRegression
from typing import Optional, Callable, Dict, Union


class IndexUnkHandler:
    def __init__(self) -> None:
        super().__init__()

    def __call__(self, word: str) -> Optional[int]:
        """
        :param word: the encountered OOV word
        :return: an index in vocabulary or None to ignore this OOV
        """
        raise NotImplementedError()


def _return_none(x: str) -> None:
    return None


def _fetch_unk_handler(name: str) -> Callable[[str], Optional[int]]:
    if name == 'drop':
        return _return_none
    raise ValueError()


class NamedUnkHandler(IndexUnkHandler):
    def __init__(self, unk_handler_name: str) -> None:
        super().__init__()
        self.handler = _fetch_unk_handler(unk_handler_name)

    def __call__(self, word: str) -> Optional[int]:
        return self.handler(word)


class ConstantUnkHandler(IndexUnkHandler):
    def __init__(self, constant: Optional[int]) -> None:
        super().__init__()
        self.constant = constant

    def __call__(self, word: str) -> Optional[int]:
        return self.constant


class TensorUnkHandler:
    def __init__(self) -> None:
        super().__init__()

    def __call__(self, original_token: str, token_index: int,
                 context_embedding: torch.FloatTensor) -> torch.FloatTensor:
        """
        given word embeddings of a sentence, returns word embedding of the token_index-ith word
        :param original_token: string representation of this OOV word
        :param token_index: place of this word in the context_embedding
        :param context_embedding: tensor of size (T, C)
        :return: inferred word embedding (C, )
        """
        res = self.process(original_token, token_index, context_embedding)
        if res is None:
            return context_embedding[token_index]
        return res

    def process(self, original_token: str, token_index: int,
                context_embedding: torch.FloatTensor) -> Optional[torch.FloatTensor]:
        raise NotImplementedError

    def to(self, device: Union[str, torch.device]):
        return self

    def train(self, mode: bool = True):
        return self


class Chars2VecUnkHandler(TensorUnkHandler):
    def __init__(self, chars2vec: 'Model') -> None:
        super().__init__()
        assert chars2vec.is_sentence_embedding  # make sure it pools
        self.chars2vec = chars2vec

    def to(self, device: Union[str, torch.device]):
        self.chars2vec.to(device)
        return self

    def train(self, mode: bool = True):
        self.chars2vec.train(mode)
        return self

    def process(self, original_token: str, token_index: int, context_embedding: torch.FloatTensor) -> torch.FloatTensor:
        res = self.chars2vec([original_token])[0]
        if self.chars2vec.return_numpy:
            return torch.from_numpy(res).to(context_embedding.device)
        return res


class ContextMeanUnkHandler(TensorUnkHandler):
    def __init__(self, context_size: int, exclude_self: bool = False) -> None:
        super().__init__()
        self.context_size = context_size
        self.exclude_self = exclude_self

    def process(self, original_token: str, token_index: int,
                context_embedding: torch.FloatTensor) -> Optional[torch.FloatTensor]:
        seq_len = context_embedding.shape[0]
        start = max(0, token_index - self.context_size)
        end = min(seq_len, token_index + self.context_size + 1)
        if not self.exclude_self:
            res = context_embedding[start:end]
        else:
            if start == token_index and end == (token_index + 1):
                return None
            if start == token_index:
                res = context_embedding[token_index + 1:end]
            elif end == (token_index + 1):
                res = context_embedding[start:token_index]
            else:
                res = torch.cat([context_embedding[start:token_index], context_embedding[token_index + 1:end]], dim=0)
        return torch.mean(res, dim=0)


class SkipThoughtUnkHandler(TensorUnkHandler):
    def __init__(self, trained_embedding: np.array, trained_wtoi: Dict[str, int],
                 base_embedding: np.array, base_wtoi: Dict[str, int]) -> None:
        super().__init__()
        self.base_embedding = base_embedding
        X = []
        Y = []
        base_wtoi_set = set(base_wtoi.keys())
        for word in trained_wtoi.keys():
            if word in base_wtoi:
                X.append(base_embedding[base_wtoi[word]])
                Y.append(trained_embedding[trained_wtoi[word]])
                base_wtoi_set.remove(word)
        self.lreg = LinearRegression()
        self.lreg.fit(X, Y)
        self.base_wtoi = {k: v for k, v in base_wtoi.items() if k in base_wtoi_set}

    def process(self, original_token: str, token_index: int, context_embedding: torch.FloatTensor) \
            -> Optional[torch.FloatTensor]:
        if original_token in self.base_wtoi:
            res = self.lreg.predict([self.base_embedding[self.base_wtoi[original_token]]])[0]
            return torch.from_numpy(res).to(context_embedding.device)
        return None
