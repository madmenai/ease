import torch
import numpy as np
import torch.nn as nn
from typing import Optional, Dict, Union, List, Callable

from encoder.bert import BertModel


# NOTE this should be always batch_first
class AbstractEmbedding(nn.Module):
    def __init__(self, *, embedding_dim: int, is_bag: bool = False, is_one_by_one_bag: bool = False):
        super().__init__()
        self.is_bag = is_bag
        self.is_one_by_one_bag = is_one_by_one_bag
        self.embedding_dim = embedding_dim
        self.embedding = None  # NOTE it contains the word embedding so reduce embedding can read it

    def forward(self, *args) -> torch.FloatTensor:
        raise NotImplementedError()

    @property
    def data(self):
        return self.embedding.weight.data

    @data.setter
    def data(self, value):
        self.embedding.weight.data = value


class BertEmbedding(AbstractEmbedding):
    def __init__(self, bert_model: BertModel):
        super().__init__(embedding_dim=bert_model.embeddings.word_embeddings.embedding_dim)
        self.bert_embedding = bert_model.embeddings
        self.embedding = bert_model.embeddings.word_embeddings

    def forward(self, input_ids) -> torch.FloatTensor:
        return self.bert_embedding(input_ids, torch.zeros_like(input_ids))


class Embedding(AbstractEmbedding):
    def __init__(self, embedding: np.array, is_bag: bool) -> None:
        super().__init__(is_bag=is_bag, embedding_dim=embedding.shape[1])
        self.num_embeddings = embedding.shape[0]
        if is_bag:
            self.embedding = nn.EmbeddingBag(embedding.shape[0], embedding.shape[1])
        else:
            self.embedding = nn.Embedding(embedding.shape[0], embedding.shape[1])
        self.embedding.weight.data = torch.from_numpy(embedding.astype(np.float32))

    def forward(self, *args) -> torch.FloatTensor:
        return self.embedding(*args)


class EmbeddingWithPos(AbstractEmbedding):
    def __init__(self, token_embedding: np.array, pos_embedding: Optional[np.array] = None) -> None:
        super().__init__(is_bag=False, embedding_dim=token_embedding.shape[1])
        self.num_embeddings = token_embedding.shape[0]
        assert self.embedding_dim == pos_embedding.shape[1]
        self.embedding = nn.Embedding(token_embedding.shape[0], token_embedding.shape[1])
        self.embedding.weight.data = torch.from_numpy(token_embedding.astype(np.float32))
        if pos_embedding is not None:
            self.pos_embedding = nn.Embedding(pos_embedding.shape[0], pos_embedding.shape[1])
            self.pos_embedding.weight.data = torch.from_numpy(pos_embedding.astype(np.float32))
        else:
            # TODO fill it with sin/cos
            self.pos_embedding = nn.Embedding(512, pos_embedding.shape[1])

    def forward(self, inputs) -> torch.FloatTensor:
        return self.embedding(inputs) + self.pos_embedding(torch.arange(inputs.shape[1]).to(inputs.device).unsqueeze(0))


class Token2Id:
    def __init__(self) -> None:
        super().__init__()

    def __getitem__(self, item) -> Union[Optional[int], List[int]]:
        raise NotImplementedError()


class DictToken2Id(Token2Id):
    def __init__(self, dictionary: Dict[str, int]) -> None:
        super().__init__()
        self.dictionary = dictionary

    def __getitem__(self, item: str) -> Optional[int]:
        return self.dictionary.get(item, None)

    def __contains__(self, item: str) -> bool:
        return item in self.dictionary

    def __len__(self):
        return len(self.dictionary)

    def add(self, new_dictionary: Dict[str, int]) -> None:
        for new_token in new_dictionary:
            if new_token not in self.dictionary:
                self.dictionary[new_token] = new_dictionary[new_token]


class ListToken2Id(DictToken2Id):
    def __init__(self, vocab: List[str]) -> None:
        super().__init__({v: i for i, v in enumerate(vocab)})


class Token2Ids(Token2Id):
    def __init__(self, get_sub_ids: Callable[[str], List[int]]) -> None:
        super().__init__()
        self.get_sub_ids = get_sub_ids

    def __getitem__(self, item: str) -> List[int]:
        return self.get_sub_ids(item)
