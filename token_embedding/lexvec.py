import os
import sys
import torch
import struct
import numpy as np
from typing import Optional, List

from utils import download
from token_embedding.embedding import AbstractEmbedding
from token_embedding.embedding_loader import EmbeddingLoader
from token_embedding.embedding import DictToken2Id, Token2Ids

MAGIC_NUMBER = 0xbea25956
MODEL_VERSION = 1


class LexVecModel:
    """
    LexVec: Incorporating Subword Information into Matrix Factorization Word Embeddings
    paper: http://www.aclweb.org/anthology/W18-1209
    go_code: https://github.com/alexandres/lexvec
    """

    def __init__(self, path):
        self._f = open(path, 'rb')
        self._parse_header()

    def _read_int(self):
        self._bytes_read += 4
        return struct.unpack('I', self._f.read(4))[0]

    def _parse_header(self):
        self._bytes_read = 0
        magic_number = self._read_int()
        if magic_number != MAGIC_NUMBER:
            raise Exception("bad magic number %d, expected %d", magic_number, MAGIC_NUMBER)
        model_version = self._read_int()
        if model_version != MODEL_VERSION:
            raise Exception("bad version %d, expected %d", model_version, MODEL_VERSION)
        self._vocab_size = self._read_int()
        self._subword_matrix_rows = self._read_int()
        self._buckets = self._subword_matrix_rows - self._vocab_size
        self._dim = self._read_int()
        self._minn = self._read_int()
        self._maxn = self._read_int()
        self._i2w = []
        self._w2i = {}
        for i in range(self._vocab_size):
            w_len = self._read_int()
            w = self._f.read(w_len).decode('utf-8')
            self._bytes_read += w_len
            self._w2i[w] = len(self._i2w)
            self._i2w.append(w)
        self._matrix_base_offset = self._bytes_read

    def _subword_idx(self, sw):
        return self._vocab_size + self._fnv(sw.encode('utf-8')) % self._buckets

    @classmethod
    def _fnv(cls, data):
        h = 0x811c9dc5
        sh = 1 << 32
        cast = ord if sys.version_info[0] < 3 else lambda x: x
        for byte in data:
            h = (h * 0x01000193) % sh
            h = h ^ cast(byte)
        return h

    def _compute_subwords(self, unwrapped_w):
        w = "<%s>" % unwrapped_w
        if len(w) < self._minn:
            return []
        subwords = []
        for i in range(0, len(w) - self._minn + 1):
            l = self._minn
            while l < len(w) and l <= self._maxn and i + l <= len(w):
                subwords.append(w[i:i + l])
                l += 1
        return subwords

    def _get_vector(self, idx):
        self._f.seek(self._matrix_base_offset + self._dim * idx * 8)
        return np.frombuffer(self._f.read(self._dim * 8), dtype=np.float64)

    def get_subword_ids(self, w, subwords=None):
        if self._minn > 0 and subwords is None:
            subwords = self._compute_subwords(w)
        subwords = [self._subword_idx(sw) for sw in subwords]
        if w in self._w2i:
            subwords.append(self._w2i[w])
        return subwords

    def get_wv_by_ids(self, subwords):
        v = np.zeros(self._dim)
        l = 0
        for sw in subwords:
            v += self._get_vector(sw)
            l += 1
        return v / l

    def word_rep(self, w, subwords=None):
        subwords = self.get_subword_ids(w, subwords)
        return self.get_wv_by_ids(subwords)


lexvec_urls = {
    'cc_cased': 'https://www.dropbox.com/s/mrxn933chn5u37z/lexvec.commoncrawl.ngramsubwords.300d.W.pos.vectors.gz?dl=1',
    'cc_lower_no_context': 'https://www.dropbox.com/s/flh1fjynqvdsj4p/lexvec.commoncrawl.300d.W.pos.vectors.gz?dl=1',
    'cc_lower_context': 'https://www.dropbox.com/s/zkiajh6fj0hm0m7/lexvec.commoncrawl.300d.W%2BC.pos.vectors.gz?dl=1',
    'news_lower_no_context': 'https://www.dropbox.com/s/kguufyc2xcdi8yk/lexvec.enwiki%2Bnewscrawl.300d.W.pos.vectors.gz?dl=1',
    'news_lower_context': 'https://www.dropbox.com/s/u320t9bw6tzlwma/lexvec.enwiki%2Bnewscrawl.300d.W%2BC.pos.vectors.gz?dl=1'
}


class LexVecLoader(EmbeddingLoader):
    def __init__(self, *, name: str = 'cc_cased', cache: str = './cache/lexvec/', vocab_size: Optional[int] = None,
                 stoi: Optional[DictToken2Id] = None):
        assert name in lexvec_urls
        super().__init__(cache=cache, dim=300, name=name, vocab_size=vocab_size, stoi=stoi, force_skip_first_line=True)
        content = download(lexvec_urls[name], cache, 'vectors.txt' if name == 'cc_cased' else None)
        self.load_embeddings(os.path.join(cache, name + '.dat'), content)


class LexBagEmbedding(AbstractEmbedding):
    def __init__(self, model: LexVecModel, embedding_dim: int = 300):
        self.model = model
        super().__init__(embedding_dim=embedding_dim, is_bag=True, is_one_by_one_bag=True)

    def forward(self, subinds) -> torch.FloatTensor:
        return torch.from_numy(self.model.get_wv_by_ids(subinds).astype(np.float32))

    @property
    def data(self):
        raise NotImplementedError()

    @data.setter
    def data(self, value):
        raise NotImplementedError()


lexvec_urls_sub = {
    'cc_cased': 'https://www.dropbox.com/s/buix0deqlks4312/lexvec.commoncrawl.ngramsubwords.300d.W.pos.bin.gz?dl=1'
}


class LexVecBagLoader(EmbeddingLoader):
    def __init__(self, name: str, cache: str = './cache/lexvec/') -> None:
        assert name in lexvec_urls_sub
        super().__init__(cache=cache, dim=300, name=name, is_bag=True)
        model_path = download(lexvec_urls_sub[name], cache)
        self.embedding = LexBagEmbedding(LexVecModel(model_path), embedding_dim=300)

        def get_sub_ids(word: str) -> List[int]:
            return self.embedding.model.get_subword_ids(word)

        self.stoi = Token2Ids(get_sub_ids)
