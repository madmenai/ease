import torch
import numpy as np
from torch.autograd import Variable
from typing import List, Tuple, Optional, Union
from torch.nn.utils.rnn import pad_sequence as pad

from utils import set_device
from token_embedding.embedding_builder import EmbeddingBuilder
from token_embedding.embedding import Token2Id, AbstractEmbedding
from token_embedding.unk_handler import IndexUnkHandler, TensorUnkHandler


class LookUp:
    def __init__(self, *, unk_handler_index: IndexUnkHandler, embedding: Optional[AbstractEmbedding],
                 token2id: Optional[Token2Id], embedding_builder: Optional[EmbeddingBuilder] = None,
                 unk_handler_tensor: Optional[TensorUnkHandler] = None, token2vec: Optional['Model'] = None) -> None:
        """

        :param unk_handler_index: replaces oov words with another word or none
        :param embedding: word embedding to use
        :param token2id: word embedding's dictionary to use
        :param embedding_builder: optional embedding changer based on training sentences
        :param unk_handler_tensor: replaces oov tensors with another tensor
        :param token2vec: chars to word vector
        """
        self.unk_handler_index = unk_handler_index
        self.device = set_device('cpu')
        self.embedding = embedding
        self.token2id = token2id
        self.embedding_builder = embedding_builder
        self.unk_handler_tensor = unk_handler_tensor
        self.token2vec = token2vec
        self.train_mode = True
        if embedding is None or token2id is None:
            assert embedding_builder is not None

    def to(self, device: Union[str, torch.device]):
        device = set_device(device)
        self.device = device
        if self.token2vec is not None:
            self.token2vec.to(device)
        if self.unk_handler_tensor is not None:
            self.unk_handler_tensor.to(device)
        if self.embedding is not None:
            self.embedding.to(device)
        return self

    def train(self, mode: bool = True):
        if self.token2vec is not None:
            self.token2vec.train(mode)
        if self.unk_handler_tensor is not None:
            self.unk_handler_tensor.train(mode)
        if self.embedding is not None:
            self.embedding.train(mode)
        self.train_mode = mode
        return self

    def pre_process(self, sentences: List[List[str]]) -> None:
        if self.embedding_builder is not None:
            self.token2id, self.embedding = self.embedding_builder.process(sentences)
            self.embedding.to(self.device)
            self.embedding.train(self.train_mode)

    def __call__(self, sentences: List[List[str]]) -> Tuple[torch.FloatTensor, torch.LongTensor]:
        if self.token2vec is None:
            if self.embedding.is_bag:
                outputs, lengths = self.process_bag(sentences)
            else:
                outputs, lengths = self.process(sentences)
        else:
            outputs, lengths = self.process_token2vec(sentences)
        outputs = self.apply_unk(outputs, sentences)
        return outputs, lengths

    def apply_unk(self, outputs: torch.FloatTensor, sentences: List[List[str]]) -> torch.FloatTensor:
        if self.unk_handler_tensor is None:
            return outputs
        for j, sentence in enumerate(sentences):
            for i, token in enumerate(sentence):
                if self.token2id[token] is None:
                    outputs[j, i] = self.unk_handler_tensor(token, i, outputs[j])
        return outputs

    def process_token2vec(self, sentences: List[List[str]]) -> Tuple[torch.FloatTensor, torch.LongTensor]:
        orig_lengths = [len(x) for x in sentences]
        lengths = torch.Tensor(orig_lengths, device=self.device).long()
        all_tokens = []
        for sentence in sentences:
            all_tokens += sentence
        word_embeddings = self.token2vec(all_tokens)
        res = []
        start = 0
        for l in orig_lengths:
            res.append(word_embeddings[start:l])
            start = l
        outputs = pad(res, batch_first=True)
        return outputs, lengths

    def process_bag(self, sentences: List[List[str]]) -> Tuple[torch.FloatTensor, torch.LongTensor]:
        res = []
        orig_lengths = []
        for sentence in sentences:
            sen = []
            for i, token in enumerate(sentence):
                sen.append(self.token2id[token])
            if len(sen) == 0:
                raise ValueError()
            orig_lengths.append(len(sen))
            if self.embedding.is_one_by_one_bag:
                outputs = torch.stack([self.embedding(subinds) for subinds in sen], dim=0).to(self.device)
            else:
                token_subinds = np.empty([0], dtype=np.int64)
                token_offsets = [0]
                for subinds in sen:
                    token_subinds = np.concatenate((token_subinds, subinds))
                    token_offsets.append(token_offsets[-1] + len(subinds))
                token_offsets = token_offsets[:-1]
                ind = Variable(torch.LongTensor(token_subinds, device=self.device))
                offsets = Variable(torch.LongTensor(token_offsets, device=self.device))
                outputs = self.embedding(ind, offsets)
            res.append(outputs)
        lengths = torch.Tensor(orig_lengths, device=self.device).long()
        outputs = pad(res, batch_first=True)
        return outputs, lengths

    def process(self, sentences: List[List[str]]) -> Tuple[torch.FloatTensor, torch.LongTensor]:
        res = []
        for sentence in sentences:
            sen = []
            for token in sentence:
                index = self.token2id[token]
                if index is not None:
                    sen.append(index)
                else:
                    tmp = self.unk_handler_index(token)
                    if tmp is not None:
                        sen.append(tmp)
            if len(sen) == 0:
                raise ValueError()
            res.append(torch.LongTensor(sen, device=self.device))
        orig_lengths = [x.shape[0] for x in res]
        lengths = torch.Tensor(orig_lengths, device=self.device).long()
        outputs = pad(res, batch_first=True)
        outputs = self.embedding(outputs)
        return outputs, lengths
