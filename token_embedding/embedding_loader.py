import os
import json
import torch
import bcolz
import numpy as np
from fastText import load_model
from urllib.parse import urlparse
from typing import Optional, List, Union, Dict
from gensim.models.keyedvectors import KeyedVectors

from utils import download, load_pkl, save_pkl, mkdir, AddPath
from token_embedding.embedding import DictToken2Id, Embedding, Token2Ids


class EmbeddingLoader:
    def __init__(self, *, cache: str, dim: int, name: str, vocab_size: Optional[int] = None,
                 stoi: Optional[DictToken2Id] = None, is_bag: bool = False,
                 force_skip_first_line: bool = False) -> None:
        """
        loads a embedding matrix from a file or a numpy array with file backend
        :param cache: where the embedding is
        :param dim: embedding_dim
        :param name: name of the embedding (ex: 840B)
        :param vocab_size: optionally limit your vocab to the first vocab_size lines
        :param stoi: optional dictionary for words to their id
        :param is_bag: whether it's a bag embedding or a regular one
        """
        super().__init__()
        mkdir(cache)
        self.cache = cache
        self.stoi = stoi
        self.embedding = None
        self.dim = dim
        self.name = name
        self.vocab_size = vocab_size
        self.is_bag = is_bag
        self.unk_index = 0
        self.unk_token = '[UNK]'
        self.force_skip_first_line = force_skip_first_line
        assert self.vocab_size is None or self.stoi is None
        self.dict_address = os.path.join(self.cache, self.name + '_' + str(self.dim) + '.pkl')

    def _add_word(self, line_raw: str, needs_stoi: bool, stoi: Dict[str, int], vectors, i: int) -> int:
        line = line_raw.strip().split(' ', 1)  # NOTE some lines contain strange words and get deleted during strip
        w = line[0]
        v = np.fromstring(line[1], sep=' ', dtype=np.float32)
        if len(v) == self.dim:
            if needs_stoi:
                if w not in stoi:
                    vectors.append(v)
                    stoi[w] = i
                    i += 1
            else:
                if w in stoi:
                    i = stoi[w]
                    vectors[i * self.dim:(i + 1) * self.dim] = v
        return i

    def _extract(self, dat_file: str, content: Union[str, List[str]]):
        needs_stoi = self.stoi is None
        if needs_stoi:
            stoi = {self.unk_token: self.unk_index}
        else:
            stoi = self.stoi.dictionary
        vectors = bcolz.carray(np.zeros(len(stoi) * self.dim, dtype=np.float32), rootdir=dat_file, mode='w')
        if isinstance(content, list):
            i = len(stoi)
            for row in content:
                line_raw = (str(row[0]) + str(row[1]).replace('[', '').replace(']', '').replace('\n', '')).replace('  ',
                                                                                                                   ' ')
                i = self._add_word(line_raw, needs_stoi, stoi, vectors, i)
                if i == self.vocab_size:
                    break
        elif content.endswith('.txt') or content.endswith('.vec') or content.endswith('.vectors'):
            skip_first_line = content.endswith('.vec') or self.force_skip_first_line
            with open(content) as f:
                i = len(stoi)
                for line_raw in f:
                    if skip_first_line:
                        skip_first_line = False
                        continue
                    i = self._add_word(line_raw, needs_stoi, stoi, vectors, i)
                    if i == self.vocab_size:
                        break
        vectors = bcolz.carray(vectors.reshape((len(stoi), self.dim)), rootdir=dat_file, mode='w')
        vectors.flush()
        save_pkl(self.dict_address, DictToken2Id(stoi))

    def load_embeddings(self, dat_file: str, content: Union[str, List[str]]) -> None:
        if self.embedding is None and (isinstance(content, list) or not os.path.exists(dat_file)):
            self._extract(dat_file, content)
        self.stoi = load_pkl(self.dict_address)
        self.embedding = Embedding(bcolz.open(dat_file)[:], False)

    def mock_embedding(self, dim: int, stoi: Optional[DictToken2Id] = None) -> None:
        if stoi is None:
            self.stoi = DictToken2Id({self.unk_token: self.unk_index, 'a': 1, 'b': 2, 'c': 3})
        else:
            self.stoi = stoi
        self.embedding = Embedding(np.zeros((len(self.stoi), dim)), False)


numberbatch_urls = {
    'en': 'https://conceptnet.s3.amazonaws.com/downloads/2017/numberbatch/numberbatch-en-17.06.txt.gz',
    'multi': 'https://conceptnet.s3.amazonaws.com/downloads/2017/numberbatch/numberbatch-17.06.txt.gz',
    'mock': None
}


class NumberbatchLoader(EmbeddingLoader):
    def __init__(self, name: str = 'en', cache: str = './cache/numberbatch/',
                 vocab_size: Optional[int] = None, stoi: Optional[DictToken2Id] = None):
        dim = 300
        super().__init__(cache=cache, dim=dim, name=name, vocab_size=vocab_size, stoi=stoi, force_skip_first_line=True)
        assert name in numberbatch_urls
        if name != 'mock':
            content = download(numberbatch_urls[name], cache, 'numberbatch{}.txt'.format('-en' if name == 'en' else ''))
            self.load_embeddings(os.path.join(cache, name + '_' + str(dim) + '.dat'), content)
        else:
            self.mock_embedding(dim, stoi)


glove_urls = {
    '42B': 'http://nlp.stanford.edu/data/glove.42B.300d.zip',
    '840B': 'http://nlp.stanford.edu/data/glove.840B.300d.zip',
    'twitter.27B': 'http://nlp.stanford.edu/data/glove.twitter.27B.zip',
    '6B': 'http://nlp.stanford.edu/data/glove.6B.zip',
    'mock': None
}


class GloveLoader(EmbeddingLoader):
    def __init__(self, name: str = '840B', dim: int = 300, cache: str = './cache/glove/',
                 vocab_size: Optional[int] = None, stoi: Optional[DictToken2Id] = None):
        super().__init__(cache=cache, dim=dim, name=name, vocab_size=vocab_size, stoi=stoi)
        assert name in glove_urls
        if name != 'mock':
            content = download(glove_urls[name], cache, 'glove.{}.{}d.txt'.format(name, dim))
            self.load_embeddings(os.path.join(cache, name + '_' + str(dim) + '.dat'), content)
        else:
            self.mock_embedding(dim, stoi)


fast_urls = {
    'wiki-news': 'https://s3-us-west-1.amazonaws.com/fasttext-vectors/wiki-news-300d-1M.vec.zip',
    'crawl': 'https://s3-us-west-1.amazonaws.com/fasttext-vectors/crawl-300d-2M.vec.zip',
    'open-titles': 'https://s3.amazonaws.com/fair-dme/opensubtitles.skipgram.defaults.vec.zip',
    'toronto-books': 'https://s3.amazonaws.com/fair-dme/torontobooks.skipgram.defaults.vec.zip',
    'mock': None
}

multilingual_urls = {
    'German': 'https://s3-us-west-1.amazonaws.com/fasttext-vectors/wiki.de.align.vec',
    'Russian': 'https://s3-us-west-1.amazonaws.com/fasttext-vectors/wiki.ru.align.vec',
    'Swedish': 'https://s3-us-west-1.amazonaws.com/fasttext-vectors/wiki.sv.align.vec',
    'Turkish': 'https://s3-us-west-1.amazonaws.com/fasttext-vectors/wiki.tr.align.vec',
    'Arabic': 'https://s3-us-west-1.amazonaws.com/fasttext-vectors/wiki.ar.align.vec',
    'Persian': 'https://s3-us-west-1.amazonaws.com/fasttext-vectors/wiki.fa.align.vec',
    'English': 'https://s3-us-west-1.amazonaws.com/fasttext-vectors/wiki.en.align.vec',
    'Spanish': 'https://s3-us-west-1.amazonaws.com/fasttext-vectors/wiki.es.align.vec',
    'French': 'https://s3-us-west-1.amazonaws.com/fasttext-vectors/wiki.fr.align.vec',
    'Chinese': 'https://s3-us-west-1.amazonaws.com/fasttext-vectors/wiki.zh.align.vec'
}


class FastTextLoader(EmbeddingLoader):
    def __init__(self, name: str = 'crawl', cache: str = './cache/fasttext/', vocab_size: Optional[int] = None,
                 stoi: Optional[DictToken2Id] = None):
        assert name in fast_urls or name in multilingual_urls
        super().__init__(cache=cache, dim=300, name=name, vocab_size=vocab_size, stoi=stoi)
        content = download(fast_urls[name] if name in fast_urls else multilingual_urls[name], cache)
        self.load_embeddings(os.path.join(cache, name + '.dat'), content)


fast_urls_sub = {
    'wiki-news_sub': 'https://s3-us-west-1.amazonaws.com/fasttext-vectors/wiki-news-300d-1M-subword.vec.zip',
    'crawl_sub': 'https://s3-us-west-1.amazonaws.com/fasttext-vectors/crawl-300d-2M-subword.zip',
    'wiki-new': 'https://s3-us-west-1.amazonaws.com/fasttext-vectors/cc.en.300.bin.gz',
    'wiki-old': 'https://s3-us-west-1.amazonaws.com/fasttext-vectors/wiki.en.zip',
    'simple': 'https://s3-us-west-1.amazonaws.com/fasttext-vectors/wiki.simple.zip'
}


class FastTextBagLoader(EmbeddingLoader):
    def __init__(self, name: str, dim: int = 300, cache: str = './cache/fasttext/') -> None:
        assert name in fast_urls_sub
        super().__init__(cache=cache, dim=dim, name=name, is_bag=True)
        content = {'simple': 'wiki.simple.bin', 'wiki-old': 'wiki.en.bin', 'crawl_sub': 'crawl-300d-2M-subword.bin'}
        model_path = download(fast_urls_sub[name], cache, content.get(name, None))
        self.model = load_model(model_path)
        self.embedding = Embedding(self.model.get_input_matrix(), is_bag=True)

        def get_sub_ids(word: str) -> List[int]:
            return self.model.get_subwords(word)[1].tolist()

        self.stoi = Token2Ids(get_sub_ids)


# ('levy_bow2', 'bow2.words', 300, 'levy bow2', 'http://u.cs.biu.ac.il/~yogo/data/syntemb/bow2.words.bz2'),
#         ('imagenet', 'imagenet_22k_r152.vec', 2048, 'ImageNet',
#          'https://s3.amazonaws.com/fair-dme/imagenet_22k_r152.vec.zip'),

w2v_urls = {
    'google_w2v': 'https://s3.amazonaws.com/dl4j-distribution/GoogleNews-vectors-negative300.bin.gz'
}


class Word2VecLoader(EmbeddingLoader):
    def __init__(self, name: str = 'google_w2v', dim: int = 300, vocab_size: int = 300000, cache: str = './cache/w2v/',
                 is_bin: bool = True, force_skip_first_line: bool = True) -> None:
        assert name in w2v_urls
        # TODO use this instead of gensim: https://gist.github.com/ottokart/673d82402ad44e69df85
        super().__init__(cache=cache, dim=dim, name=name, vocab_size=vocab_size,
                         force_skip_first_line=force_skip_first_line)
        url = w2v_urls[name]
        zip_path = os.path.join(cache, os.path.basename(urlparse(url).path))
        bin_path = zip_path.replace('.gz', '')
        txt_path = bin_path.replace('.bin', '.txt')
        model_file = txt_path
        if not os.path.exists(txt_path):
            if is_bin and os.path.exists(bin_path):
                model_file = self.w2v_bin_to_txt(bin_path, remove_original=True)
            else:
                if not os.path.exists(zip_path):
                    model_file = download(url=url, target_dir=cache, decompress=True)
                    if is_bin:
                        model_file = self.w2v_bin_to_txt(model_file, remove_original=True)
        self.load_embeddings(model_file.replace('.txt', '.dat'), model_file)

    @staticmethod
    def w2v_bin_to_txt(file: str, remove_original: bool = False) -> str:
        new_file = file.replace('.bin', '.txt')
        model = KeyedVectors.load_word2vec_format(file, binary=True)
        model.save_word2vec_format(new_file, binary=False)
        if remove_original:
            os.remove(file)
        return new_file


skipthoughts_urls = {
    'dictionary': 'http://www.cs.toronto.edu/~rkiros/models/dictionary.txt',
    'utable': 'http://www.cs.toronto.edu/~rkiros/models/utable.npy',
    'uni_skip': 'http://www.cs.toronto.edu/~rkiros/models/uni_skip.npz',
    'btable': 'http://www.cs.toronto.edu/~rkiros/models/btable.npy',
    'bi_skip': 'http://www.cs.toronto.edu/~rkiros/models/bi_skip.npz'
}

skipthoughts_name_dict = {
    'uni_skip': 'utable',
    'bi_skip': 'btable'
}


class SkipThoughtsLoader(EmbeddingLoader):
    def __init__(self, name: str, dim: int = 620, cache: str = './cache/skipthoughts/'):
        assert name in skipthoughts_name_dict
        super().__init__(name=name, dim=dim, cache=cache)
        self.dictionary = None
        self._table_name = skipthoughts_name_dict[name]
        self._load_dictionary()
        self._load_emb_table()
        self._load_embedding()

    def _load_dictionary(self):
        dict_file = download(url=skipthoughts_urls['dictionary'], target_dir=self.cache)
        with open(dict_file, 'r') as handle:
            dict_list = handle.readlines()
        self.dictionary = {word.strip(): idx for idx, word in enumerate(dict_list)}

    def _load_emb_table(self):
        table_file = download(url=skipthoughts_urls[self._table_name], target_dir=self.cache)
        self.word_embeddigs = np.load(table_file, encoding='latin1')

    def _load_embedding(self):
        embedding_matrix = np.zeros((len(self.dictionary) + 1, self.dim), dtype=np.float32)
        for idx, word in enumerate(self.dictionary):
            word_id = self.dictionary[word]
            embedding_matrix[idx] = self.word_embeddigs[word_id]
        self.embedding = Embedding(embedding=embedding_matrix, is_bag=False)


laser_urls = {
    'network': 'https://s3.amazonaws.com/laser-toolkit/models/networks/blstm.ep7.9langs-v1.bpej20k.model.py',
    'map': 'https://s3.amazonaws.com/laser-toolkit/models/binarize/ep7.9langs-v1.bpej20k.bin.9xx',
    'bpe': 'https://s3.amazonaws.com/laser-toolkit/models/binarize/ep7.9langs-v1.bpej20k.codes.9xx'
}


class LaserLoader(EmbeddingLoader):
    def __init__(self, name: str = 'LASER', dim: int = 384, vocab_size: Optional[int] = None,
                 cache: str = './cache/laser/'):
        super().__init__(name=name, dim=dim, cache=cache, vocab_size=vocab_size)
        network_file = download(url=laser_urls['network'], target_dir=self.cache)
        with AddPath('./zoo/laser'):
            self.model = torch.load(network_file)
        self.embedding = Embedding(embedding=self.model.embed.weight.detach().cpu().numpy(), is_bag=True)
        map_file = download(url=laser_urls['map'], target_dir=self.cache)
        tok2idx = {}
        with open(map_file) as file:
            for line in file:
                toks = line.split()  # array with idx, word, cont
                idx = int(toks[0]) - 1  # IMPORTANT: Python indices start at 0 !!!
                word = toks[1]
                tok2idx[word] = idx
        self.stoi = DictToken2Id(tok2idx)


bpemb_available_languages = {
    'as', 'el', 'oc', 'li', 'wa', 'pdc', 'nrm', 'tum', 'ak', 'ja', 'frr', 'sl', 'vo', 'ga', 'nap',
    'myv', 'ny', 'chy', 'cu', 'bi', 'azb', 'nds', 'bar', 'id', 'tr', 'chr', 'km', 'zea', 'mdf', 'za',
    'av', 'fi', 'cy', 'mg', 'en', 'eo', 'az', 'cv', 'gu', 'qu', 'dsb', 'si', 'ckb', 'ln', 'sv',
    'bh', 'rw', 'glk', 'bs', 'pl', 'xh', 'bxr', 'bcl', 'bn', 'lad', 'eu', 'ch', 'mi', 'hak', 'la',
    've', 'mh', 'wuu', 'udm', 'ps', 'ksh', 'stq', 'cs', 'ba', 'got', 'ne', 'aa', 'hi', 'frp', 'hu',
    'ce', 'fy', 'ki', 'ie', 'szl', 'mr', 'kw', 'sw', 'ts', 'kbd', 'koi', 'it', 'hr', 'pap', 'tt',
    'so', 'ady', 'sc', 'tg', 'kbp', 'pcd', 'ff', 'fo', 'kr', 'olo', 'din', 'ku', 'fj', 'kv', 'io',
    'jv', 'gag', 'ky', 'my', 'hif', 'su', 'lv', 'sr', 'hy', 'ng', 'atj', 'bm', 'scn', 'lt', 'tpi',
    'kg', 'tcy', 'nl', 'iu', 'tl', 'ka', 'ru', 'to', 'srn', 'nv', 'et', 'or', 'csb', 'sq', 'sa',
    'sm', 'krc', 'ay', 'pa', 'war', 'lg', 'ab', 'rue', 'ks', 'gom', 'rn', 'fr', 'dz', 'ast', 'da',
    'lb', 'am', 'lbe', 'pms', 'jbo', 'be', 'se', 'lmo', 'tn', 'yo', 'dty', 'kab', 'kaa', 'lez', 'vls',
    'ilo', 'vi', 'yi', 'om', 'ko', 'tw', 'ur', 'rm', 'ar', 'kj', 'pnb', 'fa', 'sco', 'pi', 'st',
    'gl', 'min', 'mt', 'tet', 'ee', 'mn', 'an', 'tk', 'ht', 'he', 'ext', 'ace', 'ug', 'es', 'nov',
    'xmf', 'kl', 'uk', 'crh', 'gv', 'lo', 'nah', 'sg', 'ho', 'ty', 'ig', 'mk', 'ss', 'zh', 'os',
    'te', 'ceb', 'ti', 'cr', 'ta', 'ca', 'de', 'cdo', 'mwl', 'pfl', 'gan', 'jam', 'sah', 'ang', 'hsb',
    'mrj', 'haw', 'na', 'cho', 'nn', 'kk', 'th', 'is', 'pt', 'gd', 'mus', 'vep', 'arc', 'nso', 'wo',
    'ia', 'af', 'lrc', 'vec', 'bpy', 'ltg', 'bg', 'als', 'ik', 'sn', 'uz', 'kn', 'mhr', 'br', 'pam',
    'sk', 'no', 'pnt', 'mai', 'diq', 'lij', 'ro', 'ml', 'bug', 'hz', 'ha', 'co', 'rmy', 'arz', 'pag',
    'bo', 'dv', 'xal', 'pih', 'mzn', 'bjn', 'sd', 'ii', 'gn', 'fur', 'zu', 'tyv', 'new', 'ms', 'sh',
}
bpemb_url_template = 'http://cosyne.h-its.org/bpemb/{lang}/{lang}.wiki.bpe.vs{vocab_size}.d{dim}.w2v.txt.tar.gz'


class BPEmbLoader(EmbeddingLoader):
    def __init__(self, name: str = 'en', dim: int = 300, vocab_size: int = 200000,
                 cache: str = './cache/bpemb/'):
        assert name in bpemb_available_languages
        super().__init__(name=name, dim=dim, cache=cache, vocab_size=vocab_size, force_skip_first_line=True)
        base_name = '{lang}.wiki.bpe.vs{vocab_size}.d{dim}.w2v'.format(lang=name, vocab_size=vocab_size, dim=dim)
        embeddings_file = download(url=bpemb_url_template.format(lang=name, vocab_size=vocab_size, dim=dim),
                                   content='data/{}/{}.txt'.format(name, base_name), target_dir=cache)
        self.load_embeddings(dat_file=os.path.join(cache, '{}.dat'.format(base_name)), content=embeddings_file)


deepmoji_urls = {
    'vocab': 'https://raw.githubusercontent.com/bfelbo/DeepMoji/master/model/vocabulary.json'
}


class DeepMojiLoader(EmbeddingLoader):
    def __init__(self, dim: int, cache: str = './cache/deepmoji/'):
        vocab_file = download(url=deepmoji_urls['vocab'], target_dir=cache)
        with open(vocab_file) as f:
            tok2idx = json.load(f)
        stoi = DictToken2Id(tok2idx)
        super().__init__(name='deepmoji', dim=dim, cache=cache, stoi=stoi)
