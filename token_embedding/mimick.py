import os
import torch
import tarfile
import dynet as dy
from polyglot.downloader import downloader
from polyglot.mapping import Embedding as PolyEmbedding

from utils import download, mkdir, load_pkl
from token_embedding.unk_handler import TensorUnkHandler
from token_embedding.embedding_loader import EmbeddingLoader
from token_embedding.embedding import Embedding, ListToken2Id

base_link = 'https://github.com/yuvalpinter/Mimick/raw/master/mimick/models/{}-lstm-est.tar.gz'
langs = {'ar', 'bg', 'cs', 'da', 'el', 'en', 'es', 'eu', 'fa', 'he', 'hi',
         'hu', 'id', 'it', 'kk', 'lv', 'ro', 'ru', 'sv', 'ta', 'tr', 'vi', 'zh'}


class LSTMMimick:
    def __init__(self, lang='en', cache='./cache/mimick'):
        mkdir(cache)
        tar_address = download(base_link.format(lang), cache, decompress=False)
        with tarfile.open(tar_address, 'r') as g:
            g.extractall(cache)
        c2i = os.path.join(cache, 'models/{}-lstm-est.c2i'.format(lang))
        self.c2i = load_pkl(c2i, True)
        self.model = dy.Model()
        char_dim = 20
        hidden_dim = 50
        word_embedding_dim = 64
        num_lstm_layers = 1

        self.char_lookup = self.model.add_lookup_parameters((len(self.c2i), char_dim), name="ce")
        self.char_fwd_lstm = dy.LSTMBuilder(num_lstm_layers, char_dim, hidden_dim, self.model)
        self.char_bwd_lstm = dy.LSTMBuilder(num_lstm_layers, char_dim, hidden_dim, self.model)
        self.lstm_to_rep_params = self.model.add_parameters((word_embedding_dim, hidden_dim * 2), name="H")
        self.lstm_to_rep_bias = self.model.add_parameters(word_embedding_dim, name="Hb")
        self.mlp_out = self.model.add_parameters((word_embedding_dim, word_embedding_dim), name="O")
        self.mlp_out_bias = self.model.add_parameters(word_embedding_dim, name="Ob")
        self.model.populate(os.path.join(cache, 'models/{}-lstm-est.bin'.format(lang)))

    def predict_emb(self, chars):
        dy.renew_cg()
        finit = self.char_fwd_lstm.initial_state()
        binit = self.char_bwd_lstm.initial_state()
        H = dy.parameter(self.lstm_to_rep_params)
        Hb = dy.parameter(self.lstm_to_rep_bias)
        O = dy.parameter(self.mlp_out)
        Ob = dy.parameter(self.mlp_out_bias)
        pad_char = self.c2i["<*>"]
        char_ids = [pad_char] + [self.c2i[char] for char in chars] + [pad_char]
        embeddings = [self.char_lookup[cid] for cid in char_ids]
        bi_fwd_out = finit.transduce(embeddings)
        bi_bwd_out = binit.transduce(reversed(embeddings))
        rep = dy.concatenate([bi_fwd_out[-1], bi_bwd_out[-1]])
        return O * dy.tanh(H * rep + Hb) + Ob


class MimickUnkHandler(TensorUnkHandler):
    """
    Mimick: Mimicking Word Embeddings using Subword RNNs
    paper: http://www.aclweb.org/anthology/D17-1010
    dynet_code: https://github.com/yuvalpinter/Mimick
    """
    def __init__(self, lang='en', cache='./cache/mimick') -> None:
        assert lang in langs
        super().__init__()
        self.model = LSTMMimick(lang, cache)

    def process(self, original_token: str, token_index: int, context_embedding: torch.FloatTensor) -> torch.FloatTensor:
        return torch.from_numpy(self.model.predict_emb(list(original_token)).npvalue()).to(context_embedding.device)


class PolyGlotLoader(EmbeddingLoader):
    """
    polyglot: Polyglot
    python_code: https://github.com/aboSamoor/polyglot
    """
    def __init__(self, lang='en', cache='./cache/polyglot') -> None:
        super().__init__(cache=cache, dim=64, name='polyglot_{}'.format(lang))
        mkdir(cache)
        downloader.download('embeddings2.' + lang, download_dir=cache, quiet=True)
        embedding = PolyEmbedding.load(os.path.join(cache, 'embeddings2/{}/embeddings_pkl.tar.bz2'.format(lang)))
        self.unk_index = 0
        self.unk_token = '<UNK>'
        self.stoi = ListToken2Id(embedding.words)
        self.embedding = Embedding(embedding.vectors, False)
