import numpy as np
from collections import Counter
from typing import List, Optional, Tuple, Callable

from token_embedding.embedding import AbstractEmbedding, Token2Id, DictToken2Id, Embedding


# NOTE you can implement elmo pre calculation with this
class EmbeddingBuilder:
    def __init__(self) -> None:
        super().__init__()

    def process(self, sentences: List[List[str]]) -> Tuple[Token2Id, AbstractEmbedding]:
        raise NotImplementedError()


class ExpandALaCarte(EmbeddingBuilder):  # NOTE this is not complete
    def __init__(self, *, window_size: int, base_embedding: AbstractEmbedding, base_token2id: DictToken2Id,
                 max_vocab_size: Optional[int] = None, min_freq: Optional[int] = None,
                 instantiate_embedding_callable: Optional[Callable[[np.array, bool], AbstractEmbedding]] = None):
        super().__init__()
        self.window_size = window_size
        self.base_embedding = base_embedding
        self.base_token2id = base_token2id
        self.token2id = {k: base_token2id[k] for k in self.base_token2id.dictionary.keys()}
        if max_vocab_size is not None:
            assert max_vocab_size >= len(self.token2id)
        self.max_vocab_size = max_vocab_size
        self.min_freq = min_freq
        self.called = False
        if instantiate_embedding_callable is not None:
            self.instantiate_embedding_callable = instantiate_embedding_callable
        else:
            self.instantiate_embedding_callable = Embedding

    def process(self, sentences: List[List[str]]) -> Tuple[DictToken2Id, AbstractEmbedding]:
        assert not self.called
        counter = Counter()
        for sentence in sentences:
            for token in sentence:
                if token not in self.token2id:
                    counter[token] += 1
        orig_len = len(self.token2id)
        targets = set()
        for token, count in counter.most_common():
            if (self.max_vocab_size is None or len(self.token2id) < self.max_vocab_size) and (
                    self.min_freq is None or count >= self.min_freq):
                self.token2id[token] = len(self.token2id)
                targets.add(token)
            else:
                break
        # now we have base_embedding, base_token2id, corpus and targets => alacarte is possible now!
        # TODO use https://github.com/NLPrinceton/ALaCarte/blob/master/alacarte.py


class ReduceEmbedding(EmbeddingBuilder):
    def __init__(self, *, base_embedding: AbstractEmbedding, base_token2id: Token2Id, special_tokens: List[str],
                 max_vocab_size: Optional[int] = None, min_freq: Optional[int] = None,
                 instantiate_embedding_callable: Optional[
                     Callable[[np.array, bool], AbstractEmbedding]] = None) -> None:
        super().__init__()
        self.base_embedding = base_embedding
        self.base_token2id = base_token2id
        self.special_tokens = special_tokens
        try:
            self.token2id = {k: base_token2id[k] for k in special_tokens}
        except KeyError as e:
            raise e
        self.counter = Counter()
        self.max_vocab_size = max_vocab_size
        self.min_freq = min_freq
        self.called = False
        if instantiate_embedding_callable is not None:
            self.instantiate_embedding_callable = instantiate_embedding_callable
        else:
            self.instantiate_embedding_callable = Embedding

    def process(self, sentences: List[List[str]]) -> Tuple[DictToken2Id, AbstractEmbedding]:
        assert not self.called
        for sentence in sentences:
            for token in sentence:
                self.counter[token] += 1
        for token, count in self.counter.most_common():
            if (self.max_vocab_size is None or len(self.token2id) < self.max_vocab_size) and (
                    self.min_freq is None or count >= self.min_freq):
                self.token2id[token] = self.base_token2id[token]
        result_embedding = np.zeros((len(self.token2id), self.base_embedding.embedding_dim), dtype=np.float32)
        result_token2id = dict()
        for i, k in enumerate(self.token2id.keys()):
            result_embedding[i] = self.base_embedding.data[self.token2id[k]]
            result_token2id[k] = i
        self.called = True
        del self.base_embedding
        self.base_embedding = None
        return DictToken2Id(result_token2id), self.instantiate_embedding_callable(result_embedding, False)
