import torch
import random
from itertools import product
from typing import Tuple, Optional
from torch.nn.utils.rnn import pad_sequence as pad
from torch.nn.utils.rnn import pack_padded_sequence as pack
from torch.nn.utils.rnn import pad_packed_sequence as unpack

from testing import UnitTest
from utils import MaybePacked
from encoder.transformer import TransformerEncoder
from encoder.bert import BertEncoder, BertModel, BertConfig
from encoder.encoder import LSTMEncoder, CoveBaseEncoder, BaseGenSenEncoder


class TestEncoding(UnitTest):
    def __init__(self, method_name='runTest'):
        super().__init__(method_name)
        self.embedding_size = 16
        self.hidden_size = 8
        self.n_layers = 2
        self.batch_size = 4
        self.max_seq_len = 32

    @staticmethod
    def get_sequence(max_seq_len: int, batch_size: int, num_features: int, batch_first: bool, packed: bool) -> \
            Tuple[MaybePacked, torch.LongTensor]:
        sorted_lengths = reversed(sorted([random.randint(1, max_seq_len) for _ in range(batch_size)]))
        seq = [torch.randn(l, num_features) for l in sorted_lengths]
        res = pad(seq, batch_first)
        if packed:
            res = pack(res, [v.size(0) for v in seq], batch_first)
        return res, torch.LongTensor([v.size(0) for v in seq])

    def check_encoder(self, encoder, output_size: Optional[int] = None):
        output_size = self.hidden_size if output_size is None else output_size
        for batch_first, packed in product([True, False], [True, False]):
            x, lens = self.get_sequence(self.max_seq_len, self.batch_size, self.embedding_size, batch_first, packed)
            y = encoder(x, lens, batch_first)
            if packed and encoder.output_is_packed:
                assert y.data.shape == (x.data.shape[0], output_size)
                self.check_list_eq(y.batch_sizes, x.batch_sizes)
            else:
                if encoder.output_is_packed:
                    y = unpack(y, batch_first=encoder.batch_first)[0]
                if packed:
                    x = unpack(x, batch_first=batch_first)[0]
                if encoder.batch_first != batch_first:
                    y = y.permute(1, 0, 2)
                assert y.shape == (x.shape[0], x.shape[1], output_size)

    def test_lstm_encoder(self):
        for bidirectional in (True, False):
            lstm = LSTMEncoder(self.embedding_size, self.hidden_size, bidirectional, self.n_layers, drop_out=0.5)
            self.check_encoder(lstm, self.hidden_size * (2 if bidirectional else 1))

    def test_cove_encoder(self):
        for concat_intermediate, concat_final, concat_word_embedding in product([True, False], [True, False],
                                                                                [True, False]):
            if not (concat_final or concat_intermediate):
                continue
            cove = CoveBaseEncoder(concat_intermediate, concat_final, concat_word_embedding,
                                   embedding_size=self.embedding_size)
            output_size = self.embedding_size * (
                    (1 if concat_word_embedding else 0) + (2 if concat_intermediate else 0) + (
                2 if concat_final else 0))
            self.check_encoder(cove, output_size)

    def test_gensen_encoder(self):
        lstm = BaseGenSenEncoder(self.embedding_size, self.hidden_size, self.n_layers)
        self.check_encoder(lstm, self.hidden_size * 2)

    def test_transformer_encoder(self):
        for causal in [True, False]:
            transformer = TransformerEncoder(resid_pdrop=0.1, n_embd=self.embedding_size, embd_pdrop=0.1,
                                             n_layer=self.n_layers, n_head=2, attn_pdrop=0.1, n_ctx=self.max_seq_len,
                                             causal=causal)
            self.check_encoder(transformer, self.embedding_size)

    def test_bert_encoder(self):
        config = BertConfig(vocab_size=11, hidden_size=self.embedding_size, num_hidden_layers=self.n_layers,
                            num_attention_heads=2, intermediate_size=6, max_position_embeddings=self.max_seq_len,
                            type_vocab_size=2)
        model = BertModel(config)
        encoder = BertEncoder(model)
        self.check_encoder(encoder, self.embedding_size)
