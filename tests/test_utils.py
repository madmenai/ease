import torch

from utils import listify
from testing import UnitTest


class TestUtils(UnitTest):
    def test_listify(self):
        assert listify(None) == []
        assert listify((1, 2, None, 3)) == [1, 2, 3]
        thing = torch.randn(5, 5)
        out = listify(thing)
        assert isinstance(out, list)
        assert len(out) == 1
        assert (thing == out[0]).all()
