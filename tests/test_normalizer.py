from itertools import product

from testing import UnitTest
from normalization.normalization import TextNormalizer, NamedTokenizer


class TestNormalizer(UnitTest):
    def test_normalizer(self):
        sentences = ['Hello World!', '<S> this is another sentence.', '漢字 hello 123', '😂 this contains emoji']
        for lower, prefix, postfix in product([True, False], ['', '<s>'], ['', '</s>']):
            normalizer = TextNormalizer(tokenizer=NamedTokenizer(lower=lower, tokenizer_name='space'),
                                        prefix=prefix, postfix=postfix)
            output = normalizer(sentences)
            assert len(sentences) == len(output)
            if lower:
                self.check_all_elements(output, lambda x: not str.isupper(x))
            if prefix != '':
                self.check_all_elements(output, lambda x: x[0] == prefix, False)
            if postfix != '':
                self.check_all_elements(output, lambda x: x[-1] == postfix, False)
