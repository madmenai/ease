import torch
import numpy as np

from testing import UnitTest
from token_embedding.embedding import DictToken2Id
from token_embedding.embedding import EmbeddingWithPos
from token_embedding.embedding_builder import ReduceEmbedding


class TestEmbeddingBuilder(UnitTest):
    # NOTE this is not a good "unit test", it relies heavily on EmbeddingWithPos
    def test_reduce_embedding(self):
        pos_embedding = np.array([[0, 0, 0], [-3, -6, -9]])
        base_embedding = EmbeddingWithPos(token_embedding=np.array([[1, 2, 3], [2, 4, 6], [3, 6, 9]]),
                                          pos_embedding=pos_embedding)
        base_token2id = DictToken2Id({'a': 0, 'b': 1, 'c': 2})
        base_result = base_embedding(torch.LongTensor([[0, 2]]))[0]

        def instantiate_embedding_callable(embedding, is_bag):
            return EmbeddingWithPos(token_embedding=embedding, pos_embedding=pos_embedding)

        reduce = ReduceEmbedding(base_embedding=base_embedding, base_token2id=base_token2id, special_tokens=[],
                                 max_vocab_size=2, min_freq=None, is_bag=False,
                                 instantiate_embedding_callable=instantiate_embedding_callable)

        new_dict, new_embedding = reduce.process([['a', 'c'], ['c', 'b', 'a']])
        assert len(new_dict.dictionary) == 2
        assert 'a' in new_dict.dictionary and 'c' in new_dict.dictionary
        reduced_result = new_embedding(torch.LongTensor([[0, 1]]))[0]
        assert (base_result == reduced_result).all()

