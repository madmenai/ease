import torch
import numpy as np

from testing import UnitTest
from pooling.pooling import Average, PNorm, Last


class TestPooling(UnitTest):
    def test_pnorm(self):
        p_values = [2, 1, 0, -1, -2, -40, 10, float('+inf')]
        pnorm = PNorm(p_values)
        x_n = np.arange(30, dtype=np.float32).reshape(2, 3, 5) + 1.0
        x = torch.from_numpy(x_n)
        y = pnorm(x, torch.LongTensor([3, 3]), [])
        assert y.shape == (x.shape[0], len(p_values) * x.shape[2])
        for i, p in enumerate(p_values):
            assert np.allclose(np.linalg.norm(x_n, p, 1), y[:, i * x.shape[2]:(i + 1) * x.shape[2]])

    def test_avg(self):
        avg = Average()
        x = torch.FloatTensor([[[1], [1], [1], [1]], [[2], [2], [2], [2]], [[3], [3], [3], [-9]]])
        y = avg(x, torch.LongTensor([4, 4, 4]), [])
        assert (y == torch.FloatTensor([[1], [2], [0]])).all()
        y = avg(x, torch.LongTensor([4, 4, 3]), [])
        assert (y == torch.FloatTensor([[1], [2], [3]])).all()

    def test_last(self):
        last = Last()
        x = torch.FloatTensor([[[1, 2], [2, 3], [3, 4]], [[4, 5], [5, 6], [6, 7]]])
        y = last(x, torch.LongTensor([3, 3]), [])
        assert (y == torch.FloatTensor([[3, 4], [6, 7]])).all()
        y = last(x, torch.LongTensor([1, 2]), [])
        assert (y == torch.FloatTensor([[1, 2], [5, 6]])).all()
