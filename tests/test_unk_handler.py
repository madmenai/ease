import torch
import unittest
import numpy as np

from testing import UnitTest
from token_embedding.unk_handler import ContextMeanUnkHandler, SkipThoughtUnkHandler


class TestUnkHandler(UnitTest):
    def test_skipthought(self):
        unk_handler = SkipThoughtUnkHandler(trained_embedding=np.array([[0, 0, 0], [3, 3, 3]]),
                                            trained_wtoi={'0': 0, '1': 1},
                                            base_embedding=np.array([[0, 0, 0], [1, 1, 1], [2, 2, 2]]),
                                            base_wtoi={'0': 0, '1': 1, '2': 2})
        res = unk_handler.process('2', 0, torch.randn(5, 5))
        assert np.allclose(res.cpu().numpy(), np.array([6, 6, 6]))

    def test_context_mean(self):
        context = torch.FloatTensor([[1, 7], [2, 6], [3, 5], [-4, -4], [5, 3], [6, 2], [7, 1]])
        for exclude_self in [True, False]:
            unk_handler = ContextMeanUnkHandler(context_size=3, exclude_self=exclude_self)
            new_context = unk_handler('', 3, context)
            res = (24.0 / 6.0) if exclude_self else (20.0 / 7.0)
            assert (new_context == torch.FloatTensor([res, res])).all()

        context = torch.FloatTensor([[3, 5], [-4, -4], [5, 3], [6, 2]])
        for exclude_self in [True, False]:
            unk_handler = ContextMeanUnkHandler(context_size=3, exclude_self=exclude_self)
            new_context = unk_handler('', 1, context)
            res0 = (14.0 / 3.0) if exclude_self else (10.0 / 4.0)
            res1 = (10.0 / 3.0) if exclude_self else (6.0 / 4.0)
            assert (new_context == torch.FloatTensor([res0, res1])).all()
